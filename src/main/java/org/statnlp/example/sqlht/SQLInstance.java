/** Statistical Natural Language Processing System
    Copyright (C) 2014-2015  Lu, Wei

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.statnlp.example.sqlht;

import org.json.JSONObject;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseInstance;


/**
 * @author wei_lu
 *
 */
public class SQLInstance extends BaseInstance<SQLInstance, Sentence, SemanticForest>{
	
	private static final long serialVersionUID = -8190693110092491424L;
	
	
	private String table;
	
	private SQLUnit[] alignments;
	
	public JSONObject obj;
	
	public SQLInstance(int instanceId, double weight) {
		super(instanceId, weight);
	}
	
	public SQLInstance(int instanceId, double weight, Sentence input, SemanticForest output, String table) {
		super(instanceId, weight);
		this.input = input;
		this.output = output;
		this.table = table;
	}
	
	@Override
	public int size() {
		return this.input.length();
	}
	
	public SQLInstance duplicate() {
		SQLInstance result = new SQLInstance(this._instanceId, this._weight, this.input, this.output, this.table);
		return result;
	}
	
	public String getTableName() {
		return this.table;
	}

	public void setAlignments(SQLUnit[] alignments) {
		this.alignments = alignments;
	}
	
	public SQLUnit[] getAlignments() {
		return this.alignments;
	}
}
