package org.statnlp.example.sqlht;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;
import org.statnlp.example.sqlht.SHTConfig.unit_type;
import org.statnlp.example.sqlht.emb.WordEmbedding;



public class SQLReader {

//	public static void main(String args[])throws IOException{
//		
//		String filename_inst = "data/clinic/all.tokenized.json";
//		
//		SQLDataManager dm = new SQLDataManager();
//		
//		List<SQLInstance> insts = read(filename_inst, dm, true, -1);
//		
//		System.err.println(insts.size()+" instances.");
//
////		for(int k = 0; k<insts.size(); k++){
////			SQLInstance instance = insts.get(k);
////			System.err.println(k+"\t"+instance.getInstanceId());
////			System.err.println(k+"\t"+instance.getWeight());
////			System.err.println(k+"\t"+instance.getInput());
////			System.err.println(k+"\t"+Arrays.toString(instance.getOutput()));
////		}
//		
////		
////		SemanticForest forest = toForest(dm);
////		System.err.println("Okay forest created..");
////		System.err.println(forest.getAllNodes().size()+" nodes..");
//	}
	
	public static List<SQLInstance> read(String filename, SQLDataManager dm, 
			boolean isTrain, int number, boolean multiTable, WordEmbedding emb) throws IOException {
		return read(filename, dm, isTrain, number, multiTable, emb, false);
	}
	
	public static List<SQLInstance> read(String filename, SQLDataManager dm, 
			boolean isTrain, int number, boolean multiTable, WordEmbedding emb, boolean cacheJSONObj) throws IOException{
		
		
		List<SQLInstance> instances = new ArrayList<SQLInstance>();
		
		BufferedReader scan = RAWF.reader(filename);
//		scan = new Scanner(new File(filename));
		String line = null;
		int id = 1;
		int maxLen = -1;
		while((line=scan.readLine())!=null){
			
			JSONObject obj = new JSONObject(line);
			
			String question = obj.getString("question").trim().toLowerCase();
			String sql = obj.getString("sql");
			if (sql.contains("JOIN") && !multiTable) {
				continue; //ignore joint table first
			}
			
			JSONObject format = obj.getJSONObject("format");
//			System.out.println(line);
			String table = multiTable ? "mixed" :  
					SHTConfig.tables[format.getJSONArray("table").getInt(0)];
			String agg = SHTConfig.AGGS[format.getInt("sel")];
			String[] columns = new String[format.getJSONArray("agg_col").length()];
			
			for (int i  = 0; i < columns.length; i++) {
				columns[i] = SHTConfig.tables[format.getJSONArray("agg_col").getJSONArray(i).getInt(0)] + "."
						+ SHTConfig.cols[format.getJSONArray("agg_col").getJSONArray(i).getInt(0)]
						[format.getJSONArray("agg_col").getJSONArray(i).getInt(1)];
			}
			
			
			JSONArray conds = format.getJSONArray("cond");
			
			Condition[] conditions = new Condition[conds.length()];
			
			for (int i = 0; i < conditions.length; i++) {
				JSONArray current_cond = conds.getJSONArray(i);
				
				String condition_table = SHTConfig.tables[current_cond.getInt(0)];
				String condition_column = condition_table + "." + SHTConfig.cols[current_cond.getInt(0)][current_cond.getInt(1)];
				String condition_operation = SHTConfig.ops[current_cond.getInt(2)];
				String condition_value = current_cond.get(3).toString().trim().toLowerCase();
				
				if (condition_value == null || condition_value.equals("null")) {
					throw new RuntimeException("condition value is null");
				}
					
				
//				System.out.println(condition_value);
				//post-processing of condition_value
				String[] vals = condition_value.split("\\s+");
				StringBuilder sb = new StringBuilder();
				for (int j = 0; j < vals.length; j++)
					sb.append(j == 0? vals[j] : " " + vals[j]);
				condition_value = sb.toString();
				
				//System.out.println(line);
				conditions[i] = new Condition(condition_table, condition_column, condition_operation, condition_value);
			}
			
//			System.out.println(agg);
//			System.out.println(table);
//			System.out.println(Arrays.toString(columns));
//			System.out.println(Arrays.toString(conditions));
			SQL sqlObj = new SQL(agg, table, columns, conditions);
			
			String[] words = question.trim().split("\\s+");
			int len = words.length;
			WordToken[] wTokens = new WordToken[len];
			for(int k = 0; k < len; k++){
				wTokens[k] = new WordToken(words[k]);
			}
			Sentence sent = new Sentence(wTokens);
			SemanticForest tree = null;
			boolean matches = ConditionMatcher.exact_matching_ordering(sent, conditions, emb);
			if (!matches) {
				System.out.println(sent.toString());
				if (isTrain)
					continue;
			} else {
				tree =  toTree(sqlObj, dm);
//				System.out.println(Arrays.toString(conditions));
//				System.out.println(sent.toString());
//				System.out.println(tree);
			}

			SQLInstance inst = new SQLInstance(id++, 1.0, sent, tree, table);
			if (cacheJSONObj) {
				inst.obj = obj;
			}
			maxLen = Math.max(sent.length(), maxLen);
			inst.setLabeled();
			if (isTrain) {
				inst.setLabeled();
			} else {
				inst.setUnlabeled();
			}
			instances.add(inst);
			if (instances.size() == number) {
				break;
			}
		}
		scan.close();
		
		if(isTrain){
			dm.fixSemanticUnits();
		}
		System.out.println("max sentence length: " + maxLen);
		
		return instances;
	}
	
	
	public static SemanticForest[] toForest(SQLDataManager dm){
		
//		boolean checkValidPair = true;
//		boolean checkValidPair = false;
		
		ArrayList<SQLUnit> units = dm.getAllUnits();
		System.out.println("toForest all units: " + units.toString());
		
		SemanticForest[] forests = new SemanticForest[SHTConfig.tables.length]; 
		
		for (int t = 0; t < forests.length; t++) {
			ArrayList<SemanticForestNode> nodes_at_prev_depth = new ArrayList<SemanticForestNode>();

			ArrayList<SemanticForestNode> rootNodes = new ArrayList<SemanticForestNode>();
			
			for (int dIndex = 1; dIndex <= SHTConfig._SEMANTIC_COND_MAX_DEPTH; dIndex++) {
				
				ArrayList<SemanticForestNode> nodes_at_curr_depth = new ArrayList<SemanticForestNode>();
				for(int wIndex = 0; wIndex < units.size(); wIndex++){
					SQLUnit unit = units.get(wIndex);
					
					SemanticForestNode node = new SemanticForestNode(unit, dIndex, unit.getType() == unit_type.cond_val && dIndex == 1? 0 : 1);
					if (unit.getType() != unit_type.cond_col && unit.getType() != unit_type.cond_op && unit.getType() != unit_type.cond_val) continue;
					if ( unit.getType() == unit_type.cond_col &&   !unit.getName().split("\\.")[0].equals(SHTConfig.tables[t])) continue;
					
					
					
					if (unit.getType() == unit_type.cond_val && dIndex == 1)
						nodes_at_curr_depth.add(node); // should be const;
					else {
						ArrayList<SemanticForestNode> node_children_0 = new ArrayList<SemanticForestNode>();
						for(int k = 0; k<nodes_at_prev_depth.size(); k++){
							SemanticForestNode node_child = nodes_at_prev_depth.get(k);
							
							if(dm.isValidUnitPair(node.getUnit(), node_child.getUnit()))
								node_children_0.add(node_child);
						}
						if(node_children_0.size()==0){
							//ignore since the children is empty..
						} else {
							SemanticForestNode[] children0 = new SemanticForestNode[node_children_0.size()];
							for(int k = 0; k<children0.length; k++){
								children0[k] = node_children_0.get(k);
							}
//							if (t == 0 && unit.type == unit_type.cond_op && unit.getId() == 9) {
//								System.out.println(unit + " dIndex: " + dIndex);
//							}
							node.setChildren(0, children0);
							nodes_at_curr_depth.add(node);
						}
					}
				}
				nodes_at_prev_depth = nodes_at_curr_depth;
				if (dIndex % 3 == 0) {
//					if (t == 4) {
//						System.out.println("stepping in col");
//					}
					ArrayList<SemanticForestNode> col_nodes_at_prev_depth = nodes_at_prev_depth;
					for (int dColIndex = 1; dColIndex <= SHTConfig._SEMANTIC_COL_MAX_DEPTH + 1; dColIndex++) {
						ArrayList<SemanticForestNode> col_nodes_at_curr_depth = new ArrayList<SemanticForestNode>();
						for(int wIndex = 0; wIndex < units.size(); wIndex++){
							SQLUnit unit = units.get(wIndex);
//							if (t == 4) {
//								System.out.println(unit.getType());
//							}
							SemanticForestNode node = new SemanticForestNode(unit, dIndex + dColIndex, 1);
							if (unit.getType() != unit_type.column && unit.getType() != unit_type.aggregation) continue;
							if ( unit.getType() == unit_type.column &&   !unit.getName().split("\\.")[0].equals(SHTConfig.tables[t])) continue;
							
							ArrayList<SemanticForestNode> node_children_0 = new ArrayList<SemanticForestNode>();
							for(int k = 0; k< col_nodes_at_prev_depth.size(); k++){
								SemanticForestNode node_child = col_nodes_at_prev_depth.get(k);
								if(dm.isValidUnitPair(node.getUnit(), node_child.getUnit()))
									node_children_0.add(node_child);
							}
							if(node_children_0.size()==0){
								//ignore since the children is empty..
							} else {
								SemanticForestNode[] children0 = new SemanticForestNode[node_children_0.size()];
								for(int k = 0; k<children0.length; k++){
									children0[k] = node_children_0.get(k);
								}
								node.setChildren(0, children0);
								col_nodes_at_curr_depth.add(node);
							}
						}
						
//						if (t == 4) {
//							System.out.println("root plac sub e");
//						}
						col_nodes_at_prev_depth = col_nodes_at_curr_depth;
						ArrayList<SQLUnit> rootUnits = dm.getRootUnits();
						for(SemanticForestNode node : col_nodes_at_prev_depth){
							SQLUnit unit = node.getUnit();
							if(rootUnits.contains(unit)){
//								if (t== 4)
//									System.out.println(node.getName());
								rootNodes.add(node);
							}
						}
					}
				}
				
			}
//			if (t == 4) {
//				System.out.println("root place");
//			}
			SemanticForestNode root = SemanticForestNode.createRootNode(SHTConfig._SEMANTIC_FOREST_MAX_DEPTH + 1);
			SemanticForestNode[] roots = new SemanticForestNode[rootNodes.size()];
			for(int k = 0; k<roots.length; k++)
				roots[k] = rootNodes.get(k);
			root.setChildren(0, roots);
//			System.out.println("adding root: root " + roots.length);
			forests[t] = new SemanticForest(root);
		}
		
		
		
		return forests;
		
	}

	public static SemanticForest toSingleForest(SQLDataManager dm){
	
//	boolean checkValidPair = true;
//	boolean checkValidPair = false;
	
	ArrayList<SQLUnit> units = dm.getAllUnits();
	System.out.println("toSingleForest all units: " + units.toString());
	
	ArrayList<SemanticForestNode> nodes_at_prev_depth = new ArrayList<SemanticForestNode>();

	ArrayList<SemanticForestNode> rootNodes = new ArrayList<SemanticForestNode>();
	
	for (int dIndex = 1; dIndex <= SHTConfig._SEMANTIC_COND_MAX_DEPTH; dIndex++) {
		
		ArrayList<SemanticForestNode> nodes_at_curr_depth = new ArrayList<SemanticForestNode>();
		for(int wIndex = 0; wIndex < units.size(); wIndex++){
			SQLUnit unit = units.get(wIndex);
			
			SemanticForestNode node = new SemanticForestNode(unit, dIndex, unit.getType() == unit_type.cond_val && dIndex == 1? 0 : 1);
			if (unit.getType() != unit_type.cond_col && unit.getType() != unit_type.cond_op && unit.getType() != unit_type.cond_val) continue;
			
			
			
			if (unit.getType() == unit_type.cond_val && dIndex == 1)
				nodes_at_curr_depth.add(node); // should be const;
			else {
				ArrayList<SemanticForestNode> node_children_0 = new ArrayList<SemanticForestNode>();
				for(int k = 0; k<nodes_at_prev_depth.size(); k++){
					SemanticForestNode node_child = nodes_at_prev_depth.get(k);
					
					if(dm.isValidUnitPair(node.getUnit(), node_child.getUnit()))
						node_children_0.add(node_child);
				}
				if(node_children_0.size()==0){
					//ignore since the children is empty..
				} else {
					SemanticForestNode[] children0 = new SemanticForestNode[node_children_0.size()];
					for(int k = 0; k<children0.length; k++){
						children0[k] = node_children_0.get(k);
					}
//						if (t == 0 && unit.type == unit_type.cond_op && unit.getId() == 9) {
//							System.out.println(unit + " dIndex: " + dIndex);
//						}
					node.setChildren(0, children0);
					nodes_at_curr_depth.add(node);
				}
			}
		}
		nodes_at_prev_depth = nodes_at_curr_depth;
		if (dIndex % 3 == 0) {
//				if (t == 4) {
//					System.out.println("stepping in col");
//				}
			ArrayList<SemanticForestNode> col_nodes_at_prev_depth = nodes_at_prev_depth;
			for (int dColIndex = 1; dColIndex <= SHTConfig._SEMANTIC_COL_MAX_DEPTH + 1; dColIndex++) {
				ArrayList<SemanticForestNode> col_nodes_at_curr_depth = new ArrayList<SemanticForestNode>();
				for(int wIndex = 0; wIndex < units.size(); wIndex++){
					SQLUnit unit = units.get(wIndex);
//						if (t == 4) {
//							System.out.println(unit.getType());
//						}
					SemanticForestNode node = new SemanticForestNode(unit, dIndex + dColIndex, 1);
					if (unit.getType() != unit_type.column && unit.getType() != unit_type.aggregation) continue;
					
					ArrayList<SemanticForestNode> node_children_0 = new ArrayList<SemanticForestNode>();
					for(int k = 0; k< col_nodes_at_prev_depth.size(); k++){
						SemanticForestNode node_child = col_nodes_at_prev_depth.get(k);
						if(dm.isValidUnitPair(node.getUnit(), node_child.getUnit()))
							node_children_0.add(node_child);
					}
					if(node_children_0.size()==0){
						//ignore since the children is empty..
					} else {
						SemanticForestNode[] children0 = new SemanticForestNode[node_children_0.size()];
						for(int k = 0; k<children0.length; k++){
							children0[k] = node_children_0.get(k);
						}
						node.setChildren(0, children0);
						col_nodes_at_curr_depth.add(node);
					}
				}
				col_nodes_at_prev_depth = col_nodes_at_curr_depth;
				ArrayList<SQLUnit> rootUnits = dm.getRootUnits();
				for(SemanticForestNode node : col_nodes_at_prev_depth){
					SQLUnit unit = node.getUnit();
					if(rootUnits.contains(unit)){
//							if (t== 4)
//								System.out.println(node.getName());
						rootNodes.add(node);
					}
				}
			}
		}
		
	}
//		if (t == 4) {
//			System.out.println("root place");
//		}
	SemanticForestNode root = SemanticForestNode.createRootNode(SHTConfig._SEMANTIC_FOREST_MAX_DEPTH + 1);
	SemanticForestNode[] roots = new SemanticForestNode[rootNodes.size()];
	for(int k = 0; k<roots.length; k++)
		roots[k] = rootNodes.get(k);
	root.setChildren(0, roots);
//		System.out.println("adding root: root " + roots.length);
	SemanticForest forest = new SemanticForest(root);
//	System.out.println(forest);
	return forest;
	
}
	
	
	private static SemanticForest toTree(SQL sql, SQLDataManager dm) {
//		System.out.println(sql.size());
		SemanticForestNode[] nodes = new SemanticForestNode[sql.size()];
		toSemanticNode(sql, unit_type.aggregation, 0,  0, nodes, dm, 1);
		
		for(int k = nodes.length-1; k>=0; k--){
			nodes[k].setId(nodes.length-1-k);
		}
		
//		for (SemanticForestNode node: nodes) {
//			System.out.println("printing nodes: " + node.getName());
//		}
		
		//add the root unit.
		dm.recordRootUnit(nodes[0].getUnit());
		
		SemanticForestNode root = SemanticForestNode.createRootNode(SHTConfig._SEMANTIC_FOREST_MAX_DEPTH);
		root.setChildren(0, new SemanticForestNode[]{nodes[0]});
		
//		//if it's the prior instance, then set it as context independent.
//		if(id<0){
//			nodes[0].getUnit().setContextIndependent();
//		}
		
		addValidUnitPairs(root, dm);
		
		SemanticForest tree = new SemanticForest(root);
//		System.out.println(tree);
//		System.out.println("after print one tree");
		return tree;
	}
	
	private static void addValidUnitPairs(SemanticForestNode parent, SQLDataManager dm){
		SemanticForestNode[][] children = parent.getChildren();
//		System.out.println("adding valid pair: " + parent.getName() + " children len: " + children.length);
		for(int k = 0; k<children.length; k++){
			SemanticForestNode child = children[k][0];
			dm.addValidUnitPair(parent.getUnit(), child.getUnit(), k);
//			System.err.println("valid: " + parent.getUnit() + " " + child.getUnit());
			addValidUnitPairs(child, dm);
		}
	}
	
	private static void toSemanticNode(SQL sql, unit_type type, int k, int pos,  SemanticForestNode[] nodes, SQLDataManager dm, int depth){
		
		int maxDepth = SHTConfig._SEMANTIC_FOREST_MAX_DEPTH;
		
		String form = null;
		unit_type nextType = null;
		String val = null;
		int cond_left = -1;
		int cond_right = -1;
		if (type == unit_type.aggregation) {
			form = sql.aggregation;
			nextType = unit_type.column;
			k = 0; 
		}
		else if (type == unit_type.column) {
			form = sql.columns[k];
			nextType = k == sql.columns.length - 1 ? unit_type.cond_col : unit_type.column;
			k = k == sql.columns.length - 1 ? 0 : k + 1;
		}
		else if (type == unit_type.cond_col) {
			form = sql.conditions[k].column;
			nextType = unit_type.cond_op;
		}
		else if (type == unit_type.cond_op) {
			form = sql.conditions[k].operation;
			nextType = unit_type.cond_val;
		}
		else if (type == unit_type.cond_val) {
			form = SHTConfig.con;
			nextType = k != sql.conditions.length - 1 ? unit_type.cond_col: null;
			val = sql.conditions[k].val;
			cond_left = sql.conditions[k].getIdx();
			cond_right = sql.conditions[k].getEndIdx();
			k = k == sql.conditions.length - 1 ? 0 : k + 1;
		} 
		
//		System.out.println(type);
//		System.out.println(form.toString() + " " + type);
		SQLUnit unit = dm.toSemanticUnit(form, type);
		if (type == unit_type.cond_val)
			unit = new SQLValUnit(unit, val, cond_left, cond_right);
		
		if(maxDepth-depth + 1 <=0)
			throw new RuntimeException("The depth is "+depth+"!");
		
		SemanticForestNode node = new SemanticForestNode(unit, nodes.length - pos, pos == nodes.length - 1? 0 : 1);
		nodes[pos] = node;
		
		
		if (pos == nodes.length - 1) {
			
		} else {
			toSemanticNode(sql, nextType, k, pos+1, nodes, dm, depth+1);
			node.setChildren(0, new SemanticForestNode[]{nodes[pos+1]});
		}
		
	}
	
	
}
