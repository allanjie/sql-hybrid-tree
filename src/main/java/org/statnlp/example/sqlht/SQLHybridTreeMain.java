/** Statistical Natural Language Processing System
    Copyright (C) 2014-2015  Lu, Wei
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.statnlp.example.sqlht;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import org.json.JSONArray;
import org.json.JSONObject;
import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.ml.opt.GradientDescentOptimizer.BestParamCriteria;
import org.statnlp.commons.ml.opt.OptimizerFactory;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.sqlht.SHTConfig.unit_type;
import org.statnlp.example.sqlht.emb.GloveWordEmbedding;
import org.statnlp.example.sqlht.emb.PubWordEmbedding;
import org.statnlp.example.sqlht.emb.WordEmbedding;
import org.statnlp.hypergraph.DiscriminativeNetworkModel;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkConfig.StoppingCriteria;
import org.statnlp.hypergraph.NetworkModel;
import org.statnlp.hypergraph.decoding.Metric;
import org.statnlp.hypergraph.neural.GlobalNeuralNetworkParam;
import org.statnlp.hypergraph.neural.NeuralNetworkCore;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class SQLHybridTreeMain {
	
	public static int numThreads = 8;
	public static String language = "en";
	public static int numIteration = 40000;
	public static double l2 = 0.01;
	public static int trainNum = 100;
	public static int devNum = 100;
	public static int testNum = 100;
	public static boolean saveModel = false;
	public static boolean readModel = false;
	public static String modelFolder = "models/";
	public static StoppingCriteria criteria = StoppingCriteria.SMALL_RELATIVE_CHANGE;
	public static NeuralType nn = NeuralType.none;
	public static OptimizerFactory optimizer = OptimizerFactory.getLBFGSFactory();
	public static int gpuId = -1;
	public static String embedding = "polyglot";
	public static double dropout = 0.0;
	public static int embeddingSize = 64;
	public static int hiddenSize = 0;
	public static boolean evalDev = false;
	public static int evalK = 11;
	public static String type = "none";
	public static boolean fixEmbedding = false; //by default do not fix the embedding.
	public static boolean useEmbeddingFeature = false;
	public static WordEmbedding emb = null;
	public static int epochLim = 1;
	public static boolean saveGMWeightOnly = false; //if saving the graphical model weights only
	public static boolean loadPretrainedWeight = false;
	public static boolean fixPretrainedWeight = true;
	public static String optimStr = "lbfgs"; 
	public static boolean doTest = true;
	public static boolean saveEpochModel = false;
	public static boolean loadEpochModel = false;
	public static int loadEpochModelNum = -1;
	
	public static boolean useBigram = false;
	public static boolean multiTable = false;
	
	public static String matchWithWordEmbedding = "none";
	public static final boolean DEBUG = false;
	
	public static enum NeuralType {
		mlp, //comes with different type
		lstm,
		none
	}
	
	//using basic model results: set max->50. remove lowercase.
	//same for the uncased for model, set max_len to 50.
	
	public static void main(String args[]) throws IOException, InterruptedException, ClassNotFoundException{
		
		System.err.println(SQLHybridTreeMain.class.getCanonicalName());
		processArgs(args);
		
		String lang = language;
		String train_file = "data/clinic-diff/acl_train.tokenized.json";
		String dev_file = "data/clinic-diff/acl_dev.tokenized.json";
		String test_file = "data/clinic-diff/acl_test.tokenized.json";
		String g_filename = "sqlhybridgrammar.txt";
//		NetworkConfig.LOAD_MODEL_WEIGHTS = true;
		if (DEBUG) {
			train_file = "data/clinic-diff/debug.json";
//			train_file = "data/clinic-diff/acl_train.tokenized.json";
//			test_file = "data/clinic-diff/debug_test.json";;
//			dev_file = train_file;
//			l2 = 0.0;
			matchWithWordEmbedding = "none";
			multiTable = true;
			numThreads = 8;
			trainNum = 10;
			devNum = 10;
			testNum = 10;
			useBigram = true;
			evalDev= false;
			saveModel = false;
			readModel = false;
//			test_file = train_file;
		}
		
		NetworkConfig.NUM_THREADS = numThreads;
		NetworkConfig.AVOID_DUPLICATE_FEATURES = true;
		NetworkConfig.L2_REGULARIZATION_CONSTANT = l2;
		
		NetworkConfig.STOPPING_CRITERIA = criteria;
		NetworkConfig.FEATURE_TOUCH_TEST = NetworkConfig.USE_NEURAL_FEATURES ? true : false;
		
		int numIterations = numIteration;
		String modelFile = "sht_l2_"+l2+"_ubg_"+useBigram+"_multiTable_"+multiTable+"_mwe_"+matchWithWordEmbedding;
		String nnModelFile = "shtnn_"+language+"_"+ nn  + "_"+ embedding + "_" + type + "_hidd_" + hiddenSize + "_"+optimStr + "_batch" + NetworkConfig.BATCH_SIZE+"_fe" + 
				fixEmbedding+ ".m";
		NetworkModel model = null;
		List<SQLInstance> insts_test = null;
		List<SQLInstance> insts_dev = null;
		WordEmbedding emb = matchWithWordEmbedding.equals("glove")? new GloveWordEmbedding("data/glove.6B.100d.txt") : matchWithWordEmbedding.equals("med") ?  new PubWordEmbedding("data/pubmed-w2v.txt"): null;
		
		if (!readModel) {
			SQLDataManager dm = new SQLDataManager();
			
			List<SQLInstance> insts_train = SQLReader.read(train_file, dm,  true, trainNum, multiTable, emb);
			
			insts_test = SQLReader.read(test_file, dm,  false, testNum, multiTable, emb, true);
			insts_dev = SQLReader.read(dev_file, dm,  false, devNum, multiTable, emb, true); //this part is used for the old model.
			
			SQLInstance[] dev_instances = null;
			emb = null;
			if (evalDev) {
				dev_instances = new SQLInstance[insts_dev.size()];
				for(int k = 0; k<dev_instances.length; k++){
					dev_instances[k] = insts_dev.get(k);
					dev_instances[k].setUnlabeled();
				}
			}
			dm.printStat();
			int size = insts_train.size();
			
			SQLInstance train_instances[] = new SQLInstance[size];
			for(int k = 0; k<insts_train.size(); k++){
				train_instances[k] = insts_train.get(k);
				train_instances[k].setInstanceId(k);
				train_instances[k].setLabeled();
			}
			
			
			System.err.println("Read.."+train_instances.length+" instances.");
			
			HybridGrammar g = HybridGrammarReader.read(g_filename);
			
			SemanticForest[] forests_global = null;
			SemanticForest forest_global = null;
			if (!multiTable)
				forests_global = SQLReader.toForest(dm);
			else
				forest_global = SQLReader.toSingleForest(dm);
			
			List<NeuralNetworkCore> nets = new ArrayList<NeuralNetworkCore>();
			if(NetworkConfig.USE_NEURAL_FEATURES){
				if (nn == NeuralType.mlp) {
					//actually inside is bilinear;
					nets.add(new DepMLP("MultiLayerPerceptron", dm.getAllUnits().size(), hiddenSize, gpuId,
							embedding, fixEmbedding, dropout, lang, type, 1)
							.setModelFile(modelFolder + nnModelFile));
				} else if (nn == NeuralType.lstm) {
					nets.add(new GeoLSTM("SimpleBiLSTM", dm.getAllUnits().size(), hiddenSize, gpuId,
							embedding, fixEmbedding, lang, type)
							.setModelFile(modelFolder + nnModelFile));
				} else {
					throw new RuntimeException("unknown nn type: " + nn);
				}
				
			} 
			GlobalNetworkParam gnp = new GlobalNetworkParam(optimizer, new GlobalNeuralNetworkParam(nets));
			
			SHTFeatureManager fm = new SHTFeatureManager(gnp, g, dm, useBigram);
			
			
			NetworkCompiler compiler = multiTable ?  
					new SHTMultiTableNetworkCompiler(g, dm, forest_global)  :
					new SHTNetworkCompiler(g, dm, forests_global); 
			
			model = DiscriminativeNetworkModel.create(fm, compiler);
			model.temp_model_file = modelFolder + modelFile;
			if (evalDev) {
				Function<Instance[], Metric> evalFunc = new Function<Instance[], Metric>(){
					@Override
					public Metric apply(Instance[] t) {
						try {
							return evaluate(t, null);
						} catch (IOException e) {
							e.printStackTrace();
						}
						return null;
					}
				};
				model.train(train_instances, numIterations, dev_instances, evalFunc, evalK);
			} else {
				model.train(train_instances, numIterations);
//				gnp.getStringIndex().buildReverseIndex();
//				StringIndex strIdx = gnp.getStringIndex();
//				strIdx.buildReverseIndex();
//				gnp.setStoreFeatureReps();
//				for (int i = 0; i < gnp.size(); i++) {
//					int[] fs = gnp.getFeatureRep(i);
//					String type = strIdx.get(fs[0]);
//					String output = strIdx.get(fs[1]);
//					String input = strIdx.get(fs[2]);
//					System.out.println("["+type+", " + output +", " + input + "]" );
//				}
			}
		} else {
			System.out.println("[Info] Reading the model..");
			String file = modelFolder + modelFile;
			if (loadEpochModel) file += loadEpochModelNum;
			ObjectInputStream ois = RAWF.objectReader(file);
			model = (NetworkModel)ois.readObject();
			ois.close();
			SHTFeatureManager sfm = (SHTFeatureManager)model.getFeatureManager();
			insts_test = SQLReader.read(test_file, sfm._dm,  false, testNum, multiTable, emb, true);
			emb = null;
		}
		
		if (saveModel) {
			//save both stuff
			System.out.println("[Info] Saving the model...");
			ObjectOutputStream oos =  RAWF.objectWriter(modelFolder + modelFile);
			if (NetworkConfig.USE_NEURAL_FEATURES)
				model.getFeatureManager().getParam_G().getNNParamG().getAllNets().get(0).setModelFile(modelFolder + nnModelFile);
			oos.writeObject(model);
			oos.close();
			if (saveGMWeightOnly) {
				oos =  RAWF.objectWriter(modelFolder + modelFile + ".weight");
				oos.writeObject(model.getFeatureManager().getParam_G().getWeights());
				oos.close();
			}
		}
		
		
		if (doTest) {
			SQLValUnit.EVAL = true;
			SQLInstance test_instances[];
			Instance[] output_instances_unlabeled;
			
			test_instances = new SQLInstance[insts_test.size()];
			for(int k = 0; k<test_instances.length; k++){
				test_instances[k] = insts_test.get(k);
				test_instances[k].setUnlabeled();
			}
			output_instances_unlabeled = model.test(test_instances);
			evaluate(output_instances_unlabeled, modelFolder + modelFile + ".res");
		}
		
	}
	
	public static SemMetric evaluate(Instance[] output_instances_unlabeled, String result_file) throws IOException {
		double total = output_instances_unlabeled.length;
		double corr = 0;
		
		
		
		for(int k = 0; k<output_instances_unlabeled.length; k++){
			SQLInstance output_inst_U = (SQLInstance)output_instances_unlabeled[k];
			SemanticForest output = output_inst_U.getOutput();
			boolean r = false;
			if (output != null) {
				r = output_inst_U.getOutput().equals(output_inst_U.getPrediction());
			}
			System.out.println(output_inst_U.getInstanceId()+":\t"+r);
			System.out.println("  " + output_inst_U.getInput().toString());
			if (output != null) {
				System.out.println("  gold: " + output);
			} else {
				System.out.println("  gold: null");
			}
			SemanticForest prediction = output_inst_U.getPrediction();
			System.out.println("  prediction: " + prediction);
			System.out.println();
			List<String> formats = prediction.getResultFormats();
//			System.out.println(formats);
			convertFormatsIntoJSON(formats, output_inst_U.obj, output_inst_U.getInput());
//			System.out.println(output_inst_U.obj);
			if(r){
				corr++;
			}
		}
		
		System.out.println("text accuracy="+corr/total+"="+corr+"/"+total);
		if (result_file != null) {
			PrintWriter pw = RAWF.writer(result_file);
			for(int k = 0; k<output_instances_unlabeled.length; k++){
				SQLInstance output_inst_U = (SQLInstance)output_instances_unlabeled[k];
				pw.println(output_inst_U.obj);
			}
			pw.close();
		}
		return new SemMetric(corr/total * 100);
	}
	
	private static void convertFormatsIntoJSON(List<String> formats, JSONObject obj, Sentence sent) {
		JSONObject predFormat = new JSONObject();
		JSONArray col_arr = new JSONArray();
		List<Integer> tableIds = new ArrayList<>();
		JSONArray cond_arr = new JSONArray();
		JSONArray cur_cond_arr = new JSONArray();
		
//		JSONArray sel_lr_arr = new JSONArray();
		
		for (String form : formats){
			String[] vals = form.split(":");
			if (vals[0].equals(unit_type.aggregation.name())) {
				predFormat.put("sel", findIdx(vals[1], SHTConfig.AGGS));
//				sel_lr_arr.put(findIdx(vals[1], SHTConfig.AGGS));
//				String[] val_idxs = vals[2].split(",");
//				int l1 = Integer.parseInt(val_idxs[0]);
//				int r1 = Integer.parseInt(val_idxs[1]);
//				int l2 = Integer.parseInt(val_idxs[2]);
//				int r2 = Integer.parseInt(val_idxs[3]);
//				sel_lr_arr.put(l1);
//				sel_lr_arr.put(r1);
//				sel_lr_arr.put(l2);
//				sel_lr_arr.put(r2);
//
//				predFormat.put("sel", sel_lr_arr);
			} else if (vals[0].equals(unit_type.column.name())) {
				String[] cols = vals[1].split("\\.");
				String table = cols[0];
				String column_name = cols[1];
				JSONArray single_col_arr = new JSONArray();
				int tableId = findIdx(table, SHTConfig.tables);
				int colId = findIdx(column_name, SHTConfig.cols[tableId]);
				single_col_arr.put(tableId);
				single_col_arr.put(colId);
				
//				String[] val_idxs = vals[2].split(",");
//				int l1 = Integer.parseInt(val_idxs[0]);
//				int r1 = Integer.parseInt(val_idxs[1]);
//				int l2 = Integer.parseInt(val_idxs[2]);
//				int r2 = Integer.parseInt(val_idxs[3]);
//				single_col_arr.put(l1);
//				single_col_arr.put(r1);
//				single_col_arr.put(l2);
//				single_col_arr.put(r2);
				
				
				col_arr.put(single_col_arr);
				boolean exist = false;
				for (int i = 0; i < tableIds.size(); i++) {
					if (tableIds.get(i) == tableId) exist = true;
				}
				if (!exist) {
					tableIds.add(tableId);
				}
			} else if (vals[0].equals(unit_type.cond_col.name())) {
				String[] cols = vals[1].split("\\.");
				String table = cols[0];
				String column_name = cols[1];
				int tableId = findIdx(table, SHTConfig.tables);
				int colId = findIdx(column_name, SHTConfig.cols[tableId]);
				cur_cond_arr.put(tableId);
				cur_cond_arr.put(colId);
				
				
//				String[] val_idxs = vals[2].split(",");
//				int l1 = Integer.parseInt(val_idxs[0]);
//				int r1 = Integer.parseInt(val_idxs[1]);
//				int l2 = Integer.parseInt(val_idxs[2]);
//				int r2 = Integer.parseInt(val_idxs[3]);
//				cur_cond_arr.put(l1);
//				cur_cond_arr.put(r1);
//				cur_cond_arr.put(l2);
//				cur_cond_arr.put(r2);
				
				boolean exist = false;
				for (int i = 0; i < tableIds.size(); i++) {
					if (tableIds.get(i) == tableId) exist = true;
				}
				if (!exist) {
					tableIds.add(tableId);
				}
			} else if (vals[0].equals(unit_type.cond_op.name())) {
				String op = vals[1];
				int opId = findIdx(op, SHTConfig.ops);
				cur_cond_arr.put(opId);
//				String[] val_idxs = vals[2].split(",");
//				int l1 = Integer.parseInt(val_idxs[0]);
//				int r1 = Integer.parseInt(val_idxs[1]);
//				int l2 = Integer.parseInt(val_idxs[2]);
//				int r2 = Integer.parseInt(val_idxs[3]);
//				cur_cond_arr.put(l1);
//				cur_cond_arr.put(r1);
//				cur_cond_arr.put(l2);
//				cur_cond_arr.put(r2);
			} else if (vals[0].equals(unit_type.cond_val.name())) {
				String[] idxs = vals[1].split(",");
				int left = Integer.parseInt(idxs[0]);
				int right = Integer.parseInt(idxs[1]);
				StringBuilder sb = new StringBuilder();
				for (int i = left; i<= right; i++) {
					sb.append(i == left ? sent.get(i).getForm() : " " + sent.get(i).getForm());
				}
				cur_cond_arr.put(sb.toString());
				cond_arr.put(cur_cond_arr);
				cur_cond_arr = new JSONArray();
			}
		}
		predFormat.put("agg_col", col_arr);
		predFormat.put("cond", cond_arr);
		Collections.sort(tableIds);
		JSONArray table_arr = new JSONArray();
		for (int i = 0; i < tableIds.size(); i++) {
			int val = tableIds.get(i);
			table_arr.put(val);
		}
		predFormat.put("table", table_arr);
		obj.put("pred_format", predFormat);
	}
	
	private static int findIdx(String val, String[] arr) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i].equals(val)){
				return i;
			} 
		}
		throw new RuntimeException("cannot find the value??");
	}
	
	private static void processArgs(String[] args) throws IOException, ClassNotFoundException {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("")
				.defaultHelp(true).description("Dependency-based Hybrid Tree for Semantic Parsing");
		
		/****Training configuration***/
		parser.addArgument("-t", "--thread").type(Integer.class).setDefault(numThreads).help("number of threads");
		parser.addArgument("--l2reg", "-l2").type(Double.class).setDefault(l2).help("L2 Regularization");
		parser.addArgument("--iteration", "-iter").type(Integer.class).setDefault(numIteration).help("The number of iteration.");
		parser.addArgument("--saveModel", "-sm").type(Boolean.class).setDefault(saveModel).help("saving the model");
		parser.addArgument("--readModel", "-rm").type(Boolean.class).setDefault(readModel).help("reading the model");
		
		/********Data configuration**********/
		parser.addArgument("--trainNum","-trainN").type(Integer.class).setDefault(trainNum).help("number of training");
		parser.addArgument("--devNum","-devN").type(Integer.class).setDefault(devNum).help("number of development");
		parser.addArgument("--testNum","-testN").type(Integer.class).setDefault(testNum).help("number of test data");
		
		
		parser.addArgument("--stopCriteria", "-sc").type(StoppingCriteria.class).setDefault(criteria).help("stopping criteria");
		parser.addArgument("--neural", "-nn").type(NeuralType.class).setDefault(nn).help("neural network type");
		parser.addArgument("--batch", "-b").type(Integer.class).setDefault(NetworkConfig.BATCH_SIZE).help("batch size");
		parser.addArgument("--useBatch", "-ub").type(Boolean.class).setDefault(NetworkConfig.USE_BATCH_TRAINING).help("use batch training");
		parser.addArgument("-optim", "--optimizer").type(String.class).choices("lbfgs", "sgdclip", "adam").setDefault("lbfgs").help("optimizer");
		parser.addArgument("-gi", "--gpuid").type(Integer.class).setDefault(gpuId).help("gpuid");
		parser.addArgument("-emb", "--embedding").type(String.class).choices("glove", "polyglot", "random", "fasttext").setDefault(embedding).help("embedding to use");
		parser.addArgument("-do", "--dropout").type(Double.class).setDefault(dropout).help("dropout rate for the lstm");
		parser.addArgument("-es", "--embeddingSize").type(Integer.class).setDefault(embeddingSize).help("embedding size");
		parser.addArgument("-hs", "--hiddenSize").type(Integer.class).setDefault(hiddenSize).help("hidden size");
		parser.addArgument("-ed", "--evalDev").type(Boolean.class).setDefault(evalDev).help("evaluate on dev set");
		parser.addArgument("-ef", "--evalFreq").type(Integer.class).setDefault(evalK).help("evaluation frequency");
		parser.addArgument("--type").type(String.class).setDefault(type).help("the type for MLP, can be mlp or bilinear or bilinear-mlp");
		parser.addArgument("-fe", "--fixEmbedding").type(Boolean.class).setDefault(fixEmbedding).help("if fix the embedding");
		parser.addArgument("-uef", "--useEmbFeats").type(Boolean.class).setDefault(useEmbeddingFeature).help("use the embedding value as features");
		parser.addArgument("-el", "--epochLim").type(Integer.class).setDefault(epochLim).help("evaluate the dev set after a number of epochs");
		parser.addArgument("-sgmw", "--saveGMWeight").type(Boolean.class).setDefault(saveGMWeightOnly).help("save graphical model weights only");
		parser.addArgument("-lpw", "--loadPretrainedWeight").type(Boolean.class).setDefault(loadPretrainedWeight).help("load pretrained parameters");
		parser.addArgument("-fpw", "--fixPretrainedWeight").type(Boolean.class).setDefault(fixPretrainedWeight).help("fix the pretrained feature weight");
		parser.addArgument("-dt", "--dotest").type(Boolean.class).setDefault(doTest).help("test the data");
		parser.addArgument("-regn", "--regneural").type(Boolean.class).setDefault(NetworkConfig.REGULARIZE_NEURAL_FEATURES).help("regularizing the neural features");
		parser.addArgument("-ubg", "--useBigram").type(Boolean.class).setDefault(useBigram).help("add bigram  features");
		parser.addArgument("-mt", "--multiTable").type(Boolean.class).setDefault(multiTable).help("use mutliple table");
		parser.addArgument("-sem", "--saveEpochModel").type(Boolean.class).setDefault(saveEpochModel).help("save the models in epoch");
		parser.addArgument("-lem", "--loadEpochModel").type(Boolean.class).setDefault(loadEpochModel).help("load the epcoch model");
		parser.addArgument("-lemn", "--loadEpochModelNum").type(Integer.class).setDefault(loadEpochModelNum).help("load the epoch model num");
		parser.addArgument("-mwe", "--matchWordEmbedding").type(String.class).setDefault(matchWithWordEmbedding).help("match with word embedding");
		Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        numThreads = ns.getInt("thread");
        l2 = ns.getDouble("l2reg");
        numIteration = ns.getInt("iteration");
        language = ns.getString("language");
        
        trainNum = ns.getInt("trainNum");
        devNum = ns.getInt("devNum");
        testNum = ns.getInt("testNum");
        
        saveModel = ns.getBoolean("saveModel");
        readModel = ns.getBoolean("readModel");
        criteria = (StoppingCriteria)ns.get("stopCriteria");
        nn = (NeuralType)ns.get("neural");
        if (nn != NeuralType.none) {
        	NetworkConfig.USE_NEURAL_FEATURES = true;
        	NetworkConfig.REGULARIZE_NEURAL_FEATURES = ns.getBoolean("regneural");
        	NetworkConfig.IS_INDEXED_NEURAL_FEATURES = false;
        }
        NetworkConfig.USE_BATCH_TRAINING = ns.getBoolean("useBatch");
        if (NetworkConfig.USE_BATCH_TRAINING) {
        	NetworkConfig.RANDOM_BATCH = true;
        	NetworkConfig.BATCH_SIZE = ns.getInt("batch");
        }
        gpuId = ns.getInt("gpuid");
        embedding = ns.getString("embedding");
        dropout = ns.getDouble("dropout");
        embeddingSize = ns.getInt("embeddingSize");
        hiddenSize = ns.getInt("hiddenSize");
        evalDev = ns.getBoolean("evalDev");
        evalK = ns.getInt("evalFreq");
        String optim = ns.getString("optimizer");
        switch (optim) {
        	case "lbfgs": optimizer = OptimizerFactory.getLBFGSFactory(); optimStr = "lbfgs"; break;
        	case "sgdclip": optimizer = evalDev ? OptimizerFactory.getGradientDescentFactoryUsingGradientClipping(BestParamCriteria.BEST_ON_DEV, 0.05, 5): 
        				OptimizerFactory.getGradientDescentFactoryUsingGradientClipping(BestParamCriteria.LAST_UPDATE, 0.05, 5);
        				optimStr = "sgdclip";
        							break;
        	case "adam" : optimizer = evalDev ? OptimizerFactory.getGradientDescentFactoryUsingAdaM(BestParamCriteria.BEST_ON_DEV) :
        		OptimizerFactory.getGradientDescentFactoryUsingAdaM(BestParamCriteria.LAST_UPDATE) ; 
        		optimStr = "adam";
        		break;
        	default: optimizer = OptimizerFactory.getLBFGSFactory(); break;
        }
        type = ns.getString("type");
        fixEmbedding = ns.getBoolean("fixEmbedding");
        useEmbeddingFeature = ns.getBoolean("useEmbFeats");
        epochLim = ns.getInt("epochLim");
        saveGMWeightOnly = ns.getBoolean("saveGMWeight");
        loadPretrainedWeight = ns.getBoolean("loadPretrainedWeight");
        doTest = ns.getBoolean("dotest");
        useBigram = ns.getBoolean("useBigram");
        multiTable = ns.getBoolean("multiTable");
        saveEpochModel = ns.getBoolean("saveEpochModel");
        loadEpochModel = ns.getBoolean("loadEpochModel");
        loadEpochModelNum = ns.getInt("loadEpochModelNum");
        matchWithWordEmbedding = ns.getString("matchWordEmbedding");
        for (String key : ns.getAttrs().keySet()) {
        	System.err.println(key + ": " + ns.get(key));
        }
        if (saveModel && readModel) {
        	throw new RuntimeException("cannot save model and read model at the same time.");
        }
	}
}