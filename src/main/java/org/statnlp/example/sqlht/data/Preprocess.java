package org.statnlp.example.sqlht.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;

import org.json.JSONObject;
import org.statnlp.commons.io.RAWF;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.simple.Sentence;

public class Preprocess {

	
	public static void tokenize(String text) {
		Sentence sent = new Sentence(text);
		for (String tok : sent.words()) {
			System.out.println(tok);
		}
	}
	
	public static void readAndWrite(String fileName) throws IOException {
		BufferedReader reader = RAWF.reader(fileName);
		String resFile = fileName.split("\\.")[0] + ".tokenized.json";
		PrintWriter pw = RAWF.writer(resFile);
		String line = null;
		while((line = reader.readLine()) != null) {
			JSONObject obj = new JSONObject(line);
			if (!obj.has("question_reph")) continue;
			String question = obj.getString("question_reph").trim(); 
			Sentence sent = new Sentence(question);
			String sql = obj.getString("sql");
			sql = sql.replaceAll("\"", "");
			obj.put("sql", sql);
			
//			if (question.endsWith("?")) {
//				if (question.substring(question.length()-2, question.length() - 1).equals(" "))
//					System.out.println(question);
//			}
			
			StringBuilder sb = new StringBuilder();
			List<String> words = sent.words();
			for (int i = 0; i< words.size(); i++) {
				sb.append(i == 0 ? words.get(i) : " " + words.get(i));
			}
			obj.put("question", sb.toString());
			pw.println(obj.toString());
		}
		reader.close();
		pw.close();
	}
	
	
	public static void readAndWriteFloat(String fileName) throws IOException {
		BufferedReader reader = RAWF.reader(fileName);
//		String resFile = fileName.split("\\.")[0] + ".tokenized.json";
//		PrintWriter pw = RAWF.writer(resFile);
		String line = null;
		while((line = reader.readLine()) != null) {
			JSONObject obj = new JSONObject(line);
			String question = obj.getString("question"); 
			String sql = obj.getString("sql");
			sql = sql.replaceAll("\"", "");
			obj.put("sql", sql);
//			System.out.println(sql);
			if (question.contains(".0")) {
				System.out.println(question);
//				pw.println(obj.toString());
			}
			else {
				//should be only ?
				question = question.substring(0, question.length() -1) + " ?";
				obj.put("question", question);
//				System.out.println(obj.toString());
//				pw.println(obj.toString());
			}
		}
		reader.close();
//		pw.close();
	}
	
	public static void main(String[] args) throws IOException{
//		readAndWrite("data/clinic-ext/all.json");
//		readAndWrite("data/clinic-ext/train.json");
//		readAndWrite("data/clinic-ext/dev.json");
//		readAndWrite("data/clinic-ext/test.json");
		
		readAndWrite("data/clinic-diff/acl_train.json");
		readAndWrite("data/clinic-diff/acl_dev.json");
		readAndWrite("data/clinic-diff/acl_test.json");
		
//		readAndWrite("data/clinic-ext/train.json");
		
		
//		tokenize("Calculate the maximum age of self-pay patients who had stemi as primary disease");
	}
}
