package org.statnlp.example.sqlht;

import java.util.Arrays;
import java.util.Comparator;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.sqlht.emb.WordEmbedding;

public class ConditionMatcher {

	
	public static enum match_criteria{
		exact_match,
		relax_match,
		most_similar
	}
	
	public static boolean exact_matching_ordering(Sentence sent, Condition[] conds, WordEmbedding emb) {
		int[] usedIdx = new int[sent.length()]; //check overlapping condition.
		boolean overlap = false;
		boolean[] foundExact = new boolean[conds.length];
		Arrays.fill(foundExact, false);
		for (int c = 0; c < conds.length; c++) {
//			if (!sentStr.contains(cond.val)) {
//				return false;
//			}
			Condition cond = conds[c];
			String[] ws = cond.val.split(" ");
			/****matching the span exactly****/
			int start = -1;
			boolean exact_found = false;
			for (int i = 0; i < sent.length(); i++) {
				exact_found = false;
				if (sent.get(i).getForm().equals(ws[0])) {
					start = i;
					exact_found = true;
					for (int x = 1; x < ws.length; x++) {
						if (start + x >= sent.length() || !sent.get(start + x).getForm().equals(ws[x])) 
							exact_found = false;
					}
					if (exact_found) {
						cond.setIdxs(start, start + ws.length - 1);
						for (int q = start ;q < start + ws.length; q++) {
							if (usedIdx[q]  == 1) {
								overlap = true;
							}
							usedIdx[q] = 1;
						}
						break;
					}
				}
			}
			/****END: matching the span exactly****/
			if (overlap) {
				System.out.println("overlapping");
				return false;
			}
			
			if (!exact_found) {
				foundExact[c] = false;
//				if (emb != null) {
//					int[] bestIdxs = new int[2];
//					boolean m =  wordEmbMatching(emb, sent, cond, usedIdx, bestIdxs);
//					if(m) {
//						cond.setIdxs(bestIdxs[0], bestIdxs[1]);
//					}
//					else return false;
//				} else {
//					return false;
//				}
			} else {
				foundExact[c] = true;
			}
			
		}
		for (int c = 0; c < conds.length; c++) {
			Condition cond = conds[c];
			if(!foundExact[c]) {
				if (emb != null) {
					int[] bestIdxs = new int[2];
					boolean m =  wordEmbMatching(emb, sent, cond, usedIdx, bestIdxs);
					if(m) {
						cond.setIdxs(bestIdxs[0], bestIdxs[1]);
					}
					else return false;
				} else {
					return false;
				}
			}
		}
//		System.out.println(sent.toString());
//		System.out.println(Arrays.toString(conds));
		Arrays.sort(conds, Comparator.comparing(Condition::getIdx));
		return true;
	}
	
	public static boolean relax_match(Sentence sent, Condition cond, int[] used) {
		String[] ws = cond.val.split(" ");
		int start = -1;
		boolean overlap = false;
		boolean relax_found = false;
		for (int i = 0; i < sent.length(); i++) {
			relax_found = false;
			if (sent.get(i).getForm().equalsIgnoreCase(ws[0])) {
				start = i;
				relax_found = true;
				for (int x = 1; x < ws.length; x++) {
					if (!sent.get(start + x).getForm().equalsIgnoreCase(ws[x])) 
						relax_found = false;
				}
				if (relax_found) {
					cond.setIdxs(start, start + ws.length - 1);
					for (int q = start ;q < start + ws.length; q++) {
						if (used[q]  == 1) {
							overlap = true;
						}
						used[q] = 1;
					}
					break;
				}
			}
		}
		if (overlap) {
			return false;
		}
		if (!relax_found) {
			return false;
		}
		return true;
	}
	
	
	public static boolean wordEmbMatching(WordEmbedding emb, Sentence sent, Condition cond, int[] used, int[] bestIdxs) {
		String[] ws = cond.val.split(" ");
		double[] valEncoding = new double[emb.getDimension()];
		for (int i = 0; i < ws.length; i++) {
			double[] cur = emb.getEmbedding(ws[i]);	
			for (int d = 0; d < emb.getDimension(); d++) {
				valEncoding[d] += cur[d];
			}
		}
		
		double best_cosine = -1;
		int best_left = -1;
		int best_right = -1;
		double[] span_encoding = new double[emb.getDimension()];
		for (int left = 0; left < sent.length(); left++) {
			if (used[left] == 1) continue;
			span_encoding = new double[emb.getDimension()];
			double[] cur = emb.getEmbedding(sent.get(left).getForm());	
			for (int d = 0; d < emb.getDimension(); d++) {
				span_encoding[d] += cur[d];
			}
			for (int right = left; right < sent.length(); right++) {
				if (used[right] == 1) break;
				cur = emb.getEmbedding(sent.get(right).getForm());	
				for (int d = 0; d < emb.getDimension(); d++) {
					span_encoding[d] += cur[d];
				}
				double cosine = cosine_simiarltiy(valEncoding, span_encoding);
				if (cosine> best_cosine) {
					best_left = left;
					best_right = right;
					best_cosine = cosine;
				}
			}
		}
		bestIdxs[0] = best_left;
		bestIdxs[1] = best_right;
		if (best_cosine == -1) {
			return false;
		}
		for (int idx = best_left; idx<= best_right; idx++) {
			used[idx] = 1;
		}
		return true;
	}
	
	private static double cosine_simiarltiy(double[] a, double[] b) {
		double num = 0;
		for (int i = 0; i < a.length; i++) {
			num += a[i] * b[i];
		}
		num /= norm(a);
		num /= norm(b);
		return num;
	}
	
	private static double norm(double[] a) {
		double sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i] * a[i];
		}
		return Math.sqrt(sum);
	}
	
}
