package org.statnlp.example.sqlht;

public class Condition {

	
	public String tableName;
	
	public String operation;
	
	public String column;
	
	public String val;
	
	public int idx;
	
	public int endIdx;
	
	public Condition(String tableName, String column, String operation, String val) {
		this.tableName = tableName;
		this.operation = operation;
		this.column = column;
		this.val = val;
	}

	public int getIdx() {
		return idx;
	}
	
	public int getEndIdx() {
		return endIdx;
	}



	public void setIdx(int idx) {
		this.idx = idx;
	}
	
	
	public void setIdxs(int idx, int endIdx) {
		this.idx = idx;
		this.endIdx = endIdx;
	}



	@Override
	public String toString() {
		return "Condition [tableName=" + tableName + ", operation=" + operation + ", column=" + column + ", val=" + val + ", left=" + idx + ", right=" + endIdx
				+ "]";
	}
	
}
