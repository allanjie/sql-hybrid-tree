/** Statistical Natural Language Processing System
    Copyright (C) 2014-2015  Lu, Wei

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.statnlp.example.sqlht;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.statnlp.example.sqlht.SHTConfig.unit_type;


/**
 * @author wei_lu
 *
 */
public class SemanticForestNode implements Serializable, Comparable<SemanticForestNode>{
	
	private static final long serialVersionUID = 4324674986215421825L;
	
	private int _id;
	private int _hIndex;//height
	private int _wIndex;//width
	private SQLUnit _unit;
	private SemanticForestNode[][] _children;//the size must be the same as the arity.
	private ArrayList<SemanticForestNode> _allNodes;//all nodes below this now, including the current one.
	private boolean _isRoot = false;
	private double _score=Double.NEGATIVE_INFINITY;
	private String _info;
	
	private int _left, _right; 
	
	public static SemanticForestNode createRootNode(int height){
		return new SemanticForestNode(height);
	}
	
	private SemanticForestNode(int height){
		this._hIndex = height;
		this._wIndex = 1000;
		this._children = new SemanticForestNode[1][];
		this._isRoot = true;
	}
	
	public SemanticForestNode(SQLUnit unit, int hIndex, int childrenSize){
		this._unit = unit;
		this._hIndex = hIndex;
		this._wIndex = unit.getId();
//		if(this._wIndex>1000){
//			throw new RuntimeException("You have too many units.. currently max is 1000.");
//		}
		this._children = new SemanticForestNode[childrenSize][];
	}
	
	public void setUnit(SQLUnit unit) {
		this._unit = unit;
		this._wIndex = unit.getId();
	}
	
	public void setInfo(String info){
		this._info = info;
	}
	
	public void setSpan(int left, int right){
		this._left = left;
		this._right = right;
	}
	
	public String getInfo(){
		return this._info;
	}
	
	@Override
	public boolean equals(Object o){
		if(!(o instanceof SemanticForestNode))
			return false;
		
		SemanticForestNode node = (SemanticForestNode)o;
		
		if (this.isRoot() && !node.isRoot()) return false;
		if (!this.isRoot() && node.isRoot()) return false;
		
		if(!this.isRoot() && !node.isRoot() && !this.getUnit().equals(node.getUnit())){
			return false;
		}
		return Arrays.deepEquals(this._children, node._children);
	}
	
	public void setScore(double score){
		this._score = score;
	}
	
	public double getScore(){
		return this._score;
	}
	
	public int getHeight(){
		int height = 0;
		for(SemanticForestNode[] child : this._children){
			for(SemanticForestNode c : child)
				height = Math.max(c.getHeight(), height);
		}
		return height + 1;
	}
	
	public boolean isRoot(){
		return this._isRoot;
	}
	
	public int arity(){
		if(this.isRoot()) return 1;
		return 1;
	}
	//arity is always 1
	
	public int getHIndex(){
		return this._hIndex;
	}
	
	public int getWIndex(){
		return this._wIndex;
	}
	
	public void setChildren(int index, SemanticForestNode[] child){
		try{
			this._children[index] = child;
		} catch(Exception e){
			System.err.println("xx:"+this.getUnit()+"\t"+child.length);
			for(int k = 0; k<child.length; k++){
				System.err.println(k+":"+child[k].getUnit());
			}
			throw new RuntimeException("x");
		}
	}
	
	public void clearChildren() {
		this._children = new SemanticForestNode[0][];
	}
	
	public SemanticForestNode[][] getChildren(){
		return this._children;
	}
	
	//including itself
	public int countAllChildren(){
		if(this._children == null){
			return 1;
		}
		int count = 0;
		for(SemanticForestNode[] child : this._children){
			for(SemanticForestNode c : child){
				count += c.countAllChildren();
			}
		}
		return count + 1;
	}
	
	//including itself
	public ArrayList<SemanticForestNode> getAllNodes(){
		if(this._allNodes!=null){
			return this._allNodes;
		}
		this._allNodes = new ArrayList<SemanticForestNode>();
		for(SemanticForestNode[] child : this._children){
			for(SemanticForestNode c : child){
				ArrayList<SemanticForestNode> subChildren = c.getAllNodes();
				for(SemanticForestNode subChild : subChildren){
					int index = Collections.binarySearch(this._allNodes, subChild);
					if(index<0){
						this._allNodes.add(-1-index, subChild);
					}
				}
			}
		}
		this._allNodes.add(this);
		return this._allNodes;
	}
	
	public SQLUnit getUnit(){
		return this._unit;
	}
	
	public void setId(int id){
		this._id = id;
	}
	
	public int getId() {
		return this._id;
	}
	
	public String getName() {
		if (this.isRoot())
			return "ROOT";
		return this._unit.toString()+"[h:"+this._hIndex+",w:"+this._wIndex+"]";
	}
	
	@Override
	public int compareTo(SemanticForestNode node) {
		if(this._hIndex!=node._hIndex)
			return this._hIndex - node._hIndex;
		return this._wIndex - node._wIndex;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		this.toStringHelper(-1, sb, 0);
		return sb.toString();
	}
	
	private void toStringHelper(int childId, StringBuilder sb, int indent){
		for(int k = 0; k<indent; k++)
			sb.append('\t');
		if(childId!=-1){
			sb.append("child-"+childId+":");
		}
		if(this.isRoot()){
			sb.append("ROOT"+"\t#children:"+this.countAllChildren()+"["+this.getHIndex()+","+this.getWIndex()+"]"+this.getHeight()+"\t"+Math.exp(this.getScore())+"\t"+this._info);
			sb.append('\n');
		} else {
			sb.append(this._unit.toString()+"\t#children:"+this.countAllChildren()+"["+this.getHIndex()+","+this.getWIndex()+"]"+this.getHeight()+"\t"+Math.exp(this.getScore())+"\t"+this._info);
			sb.append('\n');
		}
		
		if(this._children!=null){
			for(int i = 0; i<this._children.length; i++){
				SemanticForestNode[] child = this._children[i];
				for(int k = 0; k<indent; k++)
					sb.append('\t');
				sb.append("[");
				sb.append('\n');
				for(int k = 0; k<child.length; k++){
					SemanticForestNode c = child[k];
					c.toStringHelper(k, sb, indent+1);
				}
				for(int k = 0; k<indent; k++)
					sb.append('\t');
				sb.append("]");
				sb.append('\n');
			}
		}
	}
	
	private void linearize(List<String> format, int parent_left, int parent_right) {
		if(!this.isRoot()){
			unit_type type = this._unit.getType();
			String name = this._unit.getName();
			if (type == unit_type.cond_val) {
				SQLValUnit val = (SQLValUnit)this._unit;
				format.add(type + ":" + val.left +"," + val.right);
			} else{
				format.add(type + ":" + name + ":" + parent_left +"," + (this._left-1) + "," + (this._right+1) + "," + this._right);
			}
		}
		if (this._children != null) {
			for(int i = 0; i<this._children.length; i++){
				SemanticForestNode[] child = this._children[i];
				for(int k = 0; k<child.length; k++){
					SemanticForestNode c = child[k];
					c.linearize(format, this._left, this._right);
				}
			}
		}
	}
	
	public List<String> getLinearizedResults() {
		List<String> format = new ArrayList<>();
		this.linearize(format, this._left, this._right);
		return format;
	}
	
}


