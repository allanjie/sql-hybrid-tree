package org.statnlp.example.sqlht;

import cern.colt.Arrays;

public class SQL {

	
	public String table;
	public String[] columns;
	public String aggregation;
	public Condition[] conditions;
	
	public SQL(String aggregation, String table, String[] columns, Condition[] conds) {
		this.aggregation = aggregation;
		this.table = table;
		this.columns = columns;
		this.conditions = conds;
	}

	@Override
	public String toString() {
		return "SQL [table=" + table + ", column=" + Arrays.toString(columns) + ", aggregation=" + aggregation + ", condition="
				+ Arrays.toString(conditions) + "]";
	}
	
	public int size() {
		return 1 + columns.length + conditions.length * 3;
	}
	
}
