/** Statistical Natural Language Processing System
    Copyright (C) 2014-2015  Lu, Wei

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.statnlp.example.sqlht;

import java.io.Serializable;

import org.statnlp.example.sqlht.SHTConfig.unit_type;

public class SQLUnit implements Serializable{
	
	private static final long serialVersionUID = 802406508503541117L;
	
	
	protected String name;
	private int id;
	protected unit_type type;
	
	public SQLUnit(String name, unit_type type){
		this.name = name; //or value
		this.type = type;
	}
	
	
	public int arity(){
		return 1;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public unit_type getType() {
		return this.type;
	}
	
	@Override
	public int hashCode(){
		return (this.name.hashCode() + 7) ^
				(this.type.hashCode() + 7);
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof SQLUnit){
			SQLUnit su = (SQLUnit)o;
			return this.name.equals(su.name) && this.type == su.type;
		}
		return false;
	}
	
	@Override
	public String toString(){
		return this.name + "("+this.id+", "+this.type.name()+")";
	}

}