package org.statnlp.example.sqlht.emb;

public interface WordEmbedding {
	
	void readEmbedding(String file);
	
	double[] getEmbedding(String word);
	
	void clearEmbeddingMemory();
	
	int getDimension();
}
