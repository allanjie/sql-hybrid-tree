package org.statnlp.example.sqlht;

import org.statnlp.example.sqlht.SHTConfig.unit_type;

public class SQLValUnit extends SQLUnit {

	private static final long serialVersionUID = 7286459573982607381L;
	
	public String val;
	
	public int left;
	public int right;
	
	public static boolean EVAL = false;
	
	public SQLValUnit(String name, unit_type type, String val) {
		super(name, type);
		this.val = val;
	}
	
	public SQLValUnit(SQLUnit unit, String val, int left, int right) {
		super(unit.getName(), unit.getType());
		this.val = val;
		this.left = left;
		this.right = right;
		this.setId(unit.getId());
	}
	
	@Override
	public int hashCode(){
		if (!EVAL) {
			return (this.name.hashCode() + 7) ^
					(this.type.hashCode() + 7);
		} else {
			return (this.name.hashCode() + 7) ^
					(this.type.hashCode() + 7) ^ (this.val.hashCode() + 7);
		}
	}
	
	@Override
	public boolean equals(Object o){
		if (!EVAL) {
			if(o instanceof SQLValUnit){
				SQLValUnit su = (SQLValUnit)o;
				return this.name.equals(su.name) && this.type == su.type;
			}
			return false;
		} else {
			if(o instanceof SQLValUnit){
				SQLValUnit su = (SQLValUnit)o;
				return this.name.equals(su.name) && this.type == su.type && this.val.equals(su.val);
			}
			return false;
		}
		
	}

	@Override
	public String toString() {
		return "SQLValUnit ["+this.getName() + "("+this.getId()+") val=" + val + "]";
	}
	
	
}
