package org.statnlp.example.sqlht.wikisql;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.commons.types.WordToken;
import org.statnlp.example.sqlht.SHTConfig.unit_type;


import org.statnlp.example.sqlht.SQLDataManager;
import org.statnlp.example.sqlht.SQLUnit;
import org.statnlp.example.sqlht.SQLValUnit;
import org.statnlp.example.sqlht.SemanticForest;
import org.statnlp.example.sqlht.SemanticForestNode;


public class WikiReader {

	
	public Map<String, String[]> schmeas;
	public SQLUnit[] generalUnits;
	
	
	public WikiReader(SQLDataManager dm) {
		this.schmeas = new HashMap<>(); 
//				this.readTableSchema(schemaFile);
		this.generalUnits = this.getAllGeneralUnits(dm);
	}
	
	public static void main(String args[])throws IOException{
		
		SQLDataManager dm = new SQLDataManager();
		WikiReader reader = new WikiReader(dm);
		String data= "data/annotated/train.jsonl";
		
		reader.read(data, dm, true, 1);
	}
	
	private SQLUnit[] getAllGeneralUnits(SQLDataManager dm) {
		System.err.println("[Info] Loading general units");
		SQLUnit[] units = new SQLUnit[WikiConfig.AGGS.length + WikiConfig.ops.length + 1];
		int k = 0;
		for (String agg : WikiConfig.AGGS) {
			units[k++] = dm.toSemanticUnit(agg, unit_type.aggregation);
		}
		for (String op : WikiConfig.ops) {          
			units[k++] = dm.toSemanticUnit(op, unit_type.cond_op);
		}
		units[k++] = dm.toSemanticUnit(WikiConfig.constant, unit_type.cond_val);
		System.err.println("[Info] Loaded..");
		return units;
	}
	
	public SQLUnit[] getSpecificUnits(String[] columns, SQLDataManager dm) {
		SQLUnit[] units = new SQLUnit[columns.length * 2];
		int k = 0;
		for (String column : columns) {
			units[k++] = dm.toSemanticUnit(column, unit_type.column);
		}
		for (String column : columns) {
			units[k++] = dm.toSemanticUnit(column, unit_type.cond_col);
		}
		return units;
	}
	
	public List<WikiInstance> read(String filename, SQLDataManager dm, boolean isTrain, int number) throws IOException{
		
		List<WikiInstance> instances = new ArrayList<WikiInstance>();
		
		BufferedReader scan = RAWF.reader(filename);
//		scan = new Scanner(new File(filename));
		String line = null;
		int id = 1;
		int maxLen = -1;
		int numSkip = 0;
		while((line=scan.readLine())!=null){
			
			JSONObject obj = new JSONObject(line);
			
			JSONArray wordsArr = obj.getJSONObject("question").getJSONArray("words");
			int len = wordsArr.length();
			WordToken[] wTokens = new WordToken[len];
			for(int k = 0; k < len; k++){
				wTokens[k] = new WordToken(wordsArr.getString(k).trim());
			}
			Sentence sent = new Sentence(wTokens);
			
			String curr_tabId = obj.getString("table_id").trim();
			if (!this.schmeas.containsKey(curr_tabId)) {
				JSONArray headers = obj.getJSONObject("table").getJSONArray("header");
				String[] columns = new String[headers.length()];
				for (int i = 0; i < headers.length(); i++) {
					JSONArray words = headers.getJSONObject(i).getJSONArray("words");
					StringBuilder sb = new StringBuilder();
					for (int w = 0; w < words.length(); w++) {
						sb.append(w == 0? words.getString(w).trim() : " " + words.getString(w).trim());
					}
					columns[i] = sb.toString();
				}
				this.schmeas.put(curr_tabId, columns);
			}
			String[] columns = this.schmeas.get(curr_tabId);
			JSONObject sqlObj = obj.getJSONObject("query");
			
			int col_id = sqlObj.getInt("sel");
			
			if (col_id < 0 || col_id >= this.schmeas.get(curr_tabId).length) {
				throw new RuntimeException("column length error");
			}
			
			//agg_ops = ['', 'MAX', 'MIN', 'COUNT', 'SUM', 'AVG']
			//cond_ops = ['=', '>', '<', 'OP']
			//syms = ['SELECT', 'WHERE', 'AND', 'COL', 'TABLE', 'CAPTION', 'PAGE', 'SECTION', 'OP', 'COND', 'QUESTION', 'AGG', 'AGGOPS', 'CONDOPS']
			
			int agg_id = sqlObj.getInt("agg");
			
			JSONArray conds = sqlObj.getJSONArray("conds");
//			System.out.println(sent);
//			if (conds.length() == 0) {
//				throw new RuntimeException("no conditions");
//			}
			
			WikiCondition[] conditions = new WikiCondition[conds.length()];
			
			for (int i = 0; i < conditions.length; i++) {
				JSONArray current_cond = conds.getJSONArray(i);
				String condition_column = columns[current_cond.getInt(0)];
				String condition_operation = WikiConfig.ops[current_cond.getInt(1)];
				JSONArray arr = current_cond.getJSONObject(2).getJSONArray("words");
				StringBuilder sb = new StringBuilder();
				for (int w = 0; w < arr.length(); w++) {
					sb.append(w == 0? arr.getString(w).trim() : " " + arr.getString(w).trim());
				}
				String condition_value = sb.toString();
				
				if (condition_value == null || condition_value.equals("null")) {
					throw new RuntimeException("condition value is null");
				}
				
				conditions[i] = new WikiCondition(condition_column, condition_operation, condition_value);
			}
			
//			System.out.println(agg);
//			System.out.println(table);
//			System.out.println(Arrays.toString(columns));
//			System.out.println(Arrays.toString(conditions));
			WikiSQL myWikiSQL = new WikiSQL(WikiConfig.AGGS[agg_id], curr_tabId, columns[col_id], conditions);
			
			String sentstr = sent.toString();
			boolean skip = false;
			boolean[] used = new boolean[sent.length()];
			for (WikiCondition cond : conditions) {
				if (!sentstr.contains(cond.val)) {
					System.out.println(cond.val);
					System.out.println(sent.toString());
					throw new RuntimeException("not contain condition value?");
					
				} else {
					String[] ws = cond.val.split(" ");
					
					boolean found = false;
					int start = -1;
					int end = -1;
					for (int i = 0; i < sent.length(); i++) {
						found = false;
						if (sent.get(i).getForm().equals(ws[0])) {
							start = i;
							end = start + ws.length - 1;
							found = true;
							for (int x = 1; x < ws.length; x++) {
								if (!sent.get(start + x).getForm().equals(ws[x])) 
									found = false;
							}
							if (found) {
								for (int j = start; j <= end; j++) {
									if (used[j]) skip = true;
									used[j] = true;
								}
								cond.setIdx(start);
								break;
							}
						}
					}
					if (!found) {
						System.out.println(cond.val);
						System.out.println(sent.toString());
						throw new RuntimeException("not found the word: " + cond.val);
					}
				}
			}
			used = null;
			if (isTrain && skip) {
				numSkip++;
				continue;
			}
			
			Arrays.sort(conditions, Comparator.comparing(WikiCondition::getIdx).reversed());
			
//			System.out.println(sent.toString());
			SemanticForest tree =  toTree(myWikiSQL, dm);
//			SemanticForest forest = toForest(dm, this.getSpecificUnits(columns, dm));
//			System.out.println(forest);
			WikiInstance inst = new WikiInstance(id++, 1.0, sent, tree, columns, null);
			maxLen = Math.max(sent.length(), maxLen);
			inst.setLabeled();
			if (isTrain) {
				inst.setLabeled();
			} else {
				inst.setUnlabeled();
			}
			instances.add(inst);
			if (instances.size() == number) {
				break;
			}
		}
		scan.close();
		
//		if(isTrain){
//			dm.fixSemanticUnits();
//		}
		System.out.println("max sentence length: " + maxLen);
		System.out.println("number of sents skipped: " + numSkip);
		return instances;
	}
	
	public SemanticForest toForest(SQLDataManager dm, SQLUnit[] specificUnits){
	
		SQLUnit[] allUnits = new SQLUnit[this.generalUnits.length + specificUnits.length];
		int m =0;
		for (int i = 0; i < this.generalUnits.length; i++) allUnits[m++] = this.generalUnits[i];
		for (int i = 0; i < specificUnits.length; i++) allUnits[m++] = specificUnits[i];
		
//		System.out.println("all units");
//		for (SQLUnit unit: allUnits) {
//			System.out.println(unit);
//		}
		
		ArrayList<SemanticForestNode> nodes_at_prev_depth = new ArrayList<SemanticForestNode>();
	
		ArrayList<SemanticForestNode> rootNodes = new ArrayList<SemanticForestNode>();
		
		for (int dIndex = 0; dIndex <= WikiConfig.COND_MAX_DEPTH; dIndex++) {
			
			ArrayList<SemanticForestNode> nodes_at_curr_depth = new ArrayList<SemanticForestNode>();
			if (dIndex != 0) {
				for(int i = 0; i < allUnits.length; i++){
					SQLUnit unit =  allUnits[i];
					
					SemanticForestNode node = new SemanticForestNode(unit, dIndex, unit.getType() == unit_type.cond_val && dIndex == 1? 0 : 1);
					if (unit.getType() != unit_type.cond_col && unit.getType() != unit_type.cond_op && unit.getType() != unit_type.cond_val) continue;
					
					
					
					if (unit.getType() == unit_type.cond_val && dIndex == 1)
						nodes_at_curr_depth.add(node); // should be const;
					else {
						ArrayList<SemanticForestNode> node_children_0 = new ArrayList<SemanticForestNode>();
						for(int k = 0; k<nodes_at_prev_depth.size(); k++){
							SemanticForestNode node_child = nodes_at_prev_depth.get(k);
							
							if(this.isValid(node.getUnit(), node_child.getUnit()))
								node_children_0.add(node_child);
						}
						if(node_children_0.size()==0){
							//ignore since the children is empty..
						} else {
							SemanticForestNode[] children0 = new SemanticForestNode[node_children_0.size()];
							for(int k = 0; k<children0.length; k++){
								children0[k] = node_children_0.get(k);
							}
		//						if (t == 0 && unit.type == unit_type.cond_op && unit.getId() == 9) {
		//							System.out.println(unit + " dIndex: " + dIndex);
		//						}
							node.setChildren(0, children0);
							nodes_at_curr_depth.add(node);
						}
					}
				}
			}
			
			nodes_at_prev_depth = nodes_at_curr_depth;
			if (dIndex ==0 || dIndex % 3 == 0) {
				ArrayList<SemanticForestNode> col_nodes_at_prev_depth = nodes_at_prev_depth;
				for (int dColIndex = 1; dColIndex <= 2; dColIndex++) {
					ArrayList<SemanticForestNode> col_nodes_at_curr_depth = new ArrayList<SemanticForestNode>();
					for(int j = 0; j < allUnits.length; j++){
						SQLUnit unit =  allUnits[j];
						
//						if (unit.getType() == unit_type.column) {
//							System.out.println(unit + " dcol index: " + dColIndex);
//						}
//						
//						if (unit.getType() == unit_type.aggregation && dColIndex == 2) {
//							System.out.println(unit + " aggre dcol index: " + dColIndex);
//						}
						int numChildren = dIndex == 0 && dColIndex == 1&& unit.getType() == unit_type.column?
								0:1;
						SemanticForestNode node = new SemanticForestNode(unit, dIndex + dColIndex, numChildren);
						if (unit.getType() != unit_type.column && unit.getType() != unit_type.aggregation) continue;
						
						if (dIndex == 0 && dColIndex == 1&& unit.getType() == unit_type.column) {
							col_nodes_at_curr_depth.add(node);
						} else {
							ArrayList<SemanticForestNode> node_children_0 = new ArrayList<SemanticForestNode>();
							for(int k = 0; k< col_nodes_at_prev_depth.size(); k++){
								SemanticForestNode node_child = col_nodes_at_prev_depth.get(k);
								if(this.isValid(node.getUnit(), node_child.getUnit()))
									node_children_0.add(node_child);
							}
							if(node_children_0.size()==0){
								//ignore since the children is empty..
							} else {
								SemanticForestNode[] children0 = new SemanticForestNode[node_children_0.size()];
								for(int k = 0; k<children0.length; k++){
									children0[k] = node_children_0.get(k);
								}
								node.setChildren(0, children0);
								col_nodes_at_curr_depth.add(node);
							}
						}
					}
					col_nodes_at_prev_depth = col_nodes_at_curr_depth;
				}
				ArrayList<SQLUnit> rootUnits = dm.getRootUnits();
				for(SemanticForestNode node : col_nodes_at_prev_depth){
					SQLUnit unit = node.getUnit();
					if(rootUnits.contains(unit)){
						rootNodes.add(node);
					}
				}
			}
			
		}
		SemanticForestNode root = SemanticForestNode.createRootNode(WikiConfig.FOREST_MAX_DEPTH + 1);
		SemanticForestNode[] roots = new SemanticForestNode[rootNodes.size()];
		for(int k = 0; k<roots.length; k++)
			roots[k] = rootNodes.get(k);
		root.setChildren(0, roots);
		SemanticForest forest = new SemanticForest(root);
//		System.out.println(forest);
		return forest;
		
	}
	
	private boolean isValid(SQLUnit parent, SQLUnit child) {
		unit_type p_type = parent.getType();
		unit_type c_type = child.getType();
		if (p_type == unit_type.aggregation) {
			return c_type == unit_type.column;
			
		} else if (p_type == unit_type.column) {
			return c_type == unit_type.cond_col;
			
		} else if (p_type == unit_type.cond_col) {
			return c_type == unit_type.cond_op;
			
		} else if (p_type == unit_type.cond_op) {
			return c_type == unit_type.cond_val;
			
		} else if (p_type == unit_type.cond_val) {
			return c_type == unit_type.cond_col;
			
		} else {
			throw new RuntimeException("unknow type pair: " + p_type + " " + c_type);
		}
	}
	
	private SemanticForest toTree(WikiSQL sql, SQLDataManager dm) {
//		System.out.println(sql.size());
//		System.out.println(sql);
		SemanticForestNode[] nodes = new SemanticForestNode[sql.size()];
		toSemanticNode(sql, unit_type.aggregation, 0,  0, nodes, dm, 1);
		
		for(int k = nodes.length-1; k>=0; k--){
			nodes[k].setId(nodes.length-1-k);
		}
		
		//add the root unit.
//		System.out.println("the root is : " + nodes[0].getUnit());
		dm.recordRootUnit(nodes[0].getUnit());
		SemanticForestNode root = SemanticForestNode.createRootNode(WikiConfig.FOREST_MAX_DEPTH);
		root.setChildren(0, new SemanticForestNode[]{nodes[0]});
		
		SemanticForest tree = new SemanticForest(root);
//		System.out.println(tree);
//		System.out.println("after print one tree");
		return tree;
	}
	
	private void toSemanticNode(WikiSQL sql, unit_type type, int k, int pos,  SemanticForestNode[] nodes, SQLDataManager dm, int depth){
		
		int maxDepth = WikiConfig.FOREST_MAX_DEPTH;
		
		String form = null;
		unit_type nextType = null;
		String val = null;
		if (type == unit_type.aggregation) {
			form = sql.aggregation;
			nextType = unit_type.column;
			k = 0; 
		}
		else if (type == unit_type.column) {
			form = sql.column;
			nextType = unit_type.cond_col;
			k = 0;
		}
		else if (type == unit_type.cond_col) {
			form = sql.conditions[k].column;
			nextType = unit_type.cond_op;
		}
		else if (type == unit_type.cond_op) {
			form = sql.conditions[k].operation;
			nextType = unit_type.cond_val;
		}
		else if (type == unit_type.cond_val) {
			form = WikiConfig.constant;
			nextType = k != sql.conditions.length - 1 ? unit_type.cond_col: null;
			val = sql.conditions[k].val;
			k = k == sql.conditions.length - 1 ? 0 : k + 1;
		} 
		
//		System.out.println(type);
//		System.out.println(form.toString() + " " + type);
		SQLUnit unit = dm.toSemanticUnit(form, type);
		if (type == unit_type.cond_val)
			unit = new SQLValUnit(unit, val);
		
		if(maxDepth-depth + 1 <=0)
			throw new RuntimeException("The depth is "+depth+"!");
		
		SemanticForestNode node = new SemanticForestNode(unit, nodes.length - pos, pos == nodes.length - 1? 0 : 1);
		nodes[pos] = node;
		
		
		if (pos == nodes.length - 1) {
			
		} else {
			toSemanticNode(sql, nextType, k, pos+1, nodes, dm, depth+1);
			node.setChildren(0, new SemanticForestNode[]{nodes[pos+1]});
		}
		
	}
	
	
}
