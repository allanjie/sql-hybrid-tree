package org.statnlp.example.sqlht.wikisql;

import java.util.Arrays;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.sqlht.HybridGrammar;
import org.statnlp.example.sqlht.HybridPattern;
import org.statnlp.example.sqlht.SQLDataManager;
import org.statnlp.example.sqlht.SQLUnit;
import org.statnlp.hypergraph.FeatureArray;
import org.statnlp.hypergraph.FeatureManager;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.Network;



public class WikiFeatureManager extends FeatureManager {

	private static final long serialVersionUID = 1L;

	private HybridGrammar _g;
	protected SQLDataManager _dm;
	
	private enum FT {emission, transition, pattern, head, modifier, bow,headEmb, emitEmb, cheat};
	
	private static final boolean CHEAT = false;
	
	private boolean useBigram = false; //windows size of 3
	private int wordHalfWindowSize = 1;
	
	public WikiFeatureManager(GlobalNetworkParam param_g, HybridGrammar g, SQLDataManager dm, boolean useBigram) {
		super(param_g);
		this._g = g;
		this._dm = dm;
		this.useBigram = useBigram;
	}
	
	//eIndex, eIndex - bIndex, direction, hIndex, wIndex, pId, nodeType.ordinal(), k
	@Override
	protected FeatureArray extract_helper(Network network, int parent_k, int[] children_k, int children_k_index) {
		BaseNetwork stNetwork = (BaseNetwork)network;
		WikiInstance inst = (WikiInstance)stNetwork.getInstance();
		Sentence sent = inst.getInput();
		int[] ids_parent = network.getNodeArray(parent_k);
		//if it is a leaf, but the pattern is not W.
		if (ids_parent[4] != this._g.getw().getId() && children_k.length == 0) {
			throw new RuntimeException("xxx:"+Arrays.toString(ids_parent));
		}
		
		if (CHEAT) {
//			return FeatureArray.EMPTY;
			int[] fd = new int[1];
			if (ids_parent[3] != this._dm.getAllUnits().size()) {
				SQLUnit p_unit = this._dm.getAllUnits().get(ids_parent[3]);
				fd[0] = this._param_g.toFeature(stNetwork, FT.cheat.name(), p_unit + " " + p_unit.getType(), "" + Math.abs(network.getInstance().getInstanceId()) + " " +ids_parent[0] + "" + ids_parent[1] );
				return this.createFeatureArray(stNetwork, fd);
			} else {
				return FeatureArray.EMPTY;
			}
			
		}
		
		int paStart = ids_parent[0] - ids_parent[1];
		int paEnd = ids_parent[0];
//		unit_type type = unit_type.values()[ids_parent[2]];
		int unitId = ids_parent[3];
		int parentPatternId = ids_parent[4];
		
		
		if(ids_parent[0]==0){ //end index is 0;
			return FeatureArray.EMPTY;
		}
		if (unitId == this._dm.getAllUnits().size()) {
			//root node
			if(children_k.length==0){
				return FeatureArray.EMPTY;
			}
			int[] ids_child = stNetwork.getNodeArray(children_k[0]);
			SQLUnit c_unit = this._dm.getAllUnits().get(ids_child[3]);
			int f = this._param_g.toFeature(network, FT.transition.name(), "ROOT", c_unit.getName() + " " + c_unit.getType());
			int[] fs = new int[]{f};
			return this.createFeatureArray(stNetwork, fs);
		} else {
			HybridPattern pattern_parent = this._g.getPatternById(parentPatternId);
			SQLUnit p_unit = this._dm.getAllUnits().get(unitId);
			SQLUnit c_units[]  =  new SQLUnit[children_k.length];
			HybridPattern pattern_children[] = new HybridPattern[children_k.length];
			int cIndex = -1;
			for(int k = 0; k<children_k.length; k++){
				int[] ids_child = stNetwork.getNodeArray(children_k[k]);
				c_units[k] = this._dm.getAllUnits().get(ids_child[3]);
				pattern_children[k] = this._g.getPatternById(ids_child[4]);
				if (k == 1) {
					cIndex = ids_child[0] - ids_child[1];
				}
			}
			
			return this.extract_helper(network, p_unit, c_units, pattern_parent, pattern_children, sent, 
					paStart, cIndex, paEnd, parent_k, children_k_index, unitId);
		}
	}

	private FeatureArray extract_helper(Network network, SQLUnit p_unit, SQLUnit[] c_units, 
			HybridPattern pattern_parent, HybridPattern[] pattern_children,
			Sentence sent, int bIndex, int cIndex, int eIndex, int parent_k, int children_k_index, int p_unit_id){
		
//		if (pattern_parent.isX() &&network.getInstance().isLabeled()) {
//			System.out.println("transition: " + pattern_child);
//		}
		
		if (pattern_parent.isw()) {
			return FeatureArray.EMPTY;
		} else if (pattern_parent.isA() || pattern_parent.isB()) {
//			System.out.println(p_unit.getName() + " " +  pattern_children[0].toString());
			int f = this._param_g.toFeature(network, FT.pattern.name(), p_unit.getName() + " " + p_unit.getType(), pattern_children[0].toString());
//			System.out.println(p_unit.getName() + " " + p_unit.getType() + ", "+  pattern_children[0].toString());
//			System.out.println(bIndex + " " + eIndex);
			String input = this.getForm(pattern_children[0], 0, sent, bIndex);
//			if (input.equals("[X]")) return FeatureArray.EMPTY;
			FeatureArray fa = this.createFeatureArray(network, new int[] {f});
			FeatureArray curr = fa.addNext(this.createFeatureArray(network, this.addUnigram(network, p_unit , input)));
			
			if (this.useBigram) {
				int[] bgs = new int[2 * wordHalfWindowSize];
				int k = 0;
				for (int offset = -wordHalfWindowSize; offset < wordHalfWindowSize; offset++) {
					String w1 = this.getWord(sent, bIndex, offset);
					String w2 = this.getWord(sent, bIndex, offset + 1);
					bgs[k++] = this._param_g.toFeature(network,  FT.emission.name() + " bigram",  p_unit.getName() , w1 + " " + w2 );
				}
				assert(k == bgs.length);
				curr.addNext(this.createFeatureArray(network, bgs));
			}
			
			return fa;
		} else if (pattern_parent.isX()) {
			int f = this._param_g.toFeature(network, FT.transition.name(), p_unit.getName() +" "  + p_unit.getType() , c_units[0].getName()  + " " + c_units[0].getType());
			return this.createFeatureArray(network, new int[] {f});
		} else if (pattern_children.length==1) {
			return FeatureArray.EMPTY; 
		}else {
			String input = this.getForm(pattern_children[1], 0, sent, cIndex);
//			if (input.equals("[X]")) return FeatureArray.EMPTY;
			FeatureArray fa = this.createFeatureArray(network, this.addUnigram(network, p_unit, input));
			
			FeatureArray curr = fa;
			if (this.useBigram) {
				int[] bgs = new int[2 * wordHalfWindowSize];
				int k = 0;
				for (int offset = -wordHalfWindowSize; offset < wordHalfWindowSize; offset++) {
					String w1 = this.getWord(sent, cIndex, offset);
					String w2 = this.getWord(sent, cIndex, offset + 1);
					bgs[k++] = this._param_g.toFeature(network,  FT.emission.name() + " bigram",  p_unit.getName() + " " + p_unit.getType(), w1 + " " + w2 );
				}
				assert(k == bgs.length);
				curr.addNext(this.createFeatureArray(network, bgs));
			}
			return fa;
		}
	}
	
	private int[] addUnigram(Network network, SQLUnit output, String input) {
//		return new int[] {this._param_g.toFeature(network,  FT.emission.name(),  output, input)};
		String[] vals = output.getName().split(" ");
		int[] f = new int[vals.length];
		int k =0;
		for (String element : vals) {
			f[k++] =  this._param_g.toFeature(network,  FT.emission.name(),  element +" "  + output.getType(), input);
		}
		return f;
	}
	
	private String getForm(HybridPattern p, int offset, Sentence sent, int index){
		char c = p.getFormat(offset);
		if(c=='w' || c=='W'){
			return sent.get(index).getForm();
		}
		if(c!='X'){
			throw new RuntimeException("Invalid:"+p+"\t"+c);
		}
		return "["+c+"]";
	}
	
	private String getWord(Sentence sent, int index, int offset) {
		int target = index + offset;
		if(target == -1) {
			return "<S>";
		} else if(target == sent.length()) {
			return "</S>";
		} else if(target >= 0 && target < sent.length()) {
			return sent.get(target).getForm();
		} else {
			return "<PAD>";
		}
	}
}
