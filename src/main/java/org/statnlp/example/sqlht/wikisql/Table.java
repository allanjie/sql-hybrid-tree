package org.statnlp.example.sqlht.wikisql;

public class Table {

	private String tabId;
	private String[][] columns;
	
	public Table(String tabId, String[][] columns) {
		this.tabId = tabId;
		this.columns = columns;
	}

	public String getTabId() {
		return tabId;
	}

	public String[][] getColumns() {
		return columns;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public void setColumns(String[][] columns) {
		this.columns = columns;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Tab: "+ tabId + " [");
		for (int k = 0; k < columns.length;k++) {
			String[] col = columns[k];
			for (int i = 0; i < col.length; i++) {
				String colW = col[i];
				sb.append( i==0 ? colW : " " + colW);
			}
			sb.append(k == columns.length - 1? "" : ", ");
		}
		sb.append("]");
		return sb.toString();
	}
	
	

}
