package org.statnlp.example.sqlht.wikisql;

public class WikiConfig {

	public static final String SEP = " ### ";
	public static final String dummyUnit = "dummy";
	
	public static int FOREST_MAX_DEPTH = 14; //the max depth of the forest when creating the semantic forest.
	
	public static int COL_MAX_DEPTH = 1;
	public static int COND_MAX_DEPTH = 12;
	
	//agg_ops = ['', 'MAX', 'MIN', 'COUNT', 'SUM', 'AVG']
	//cond_ops = ['=', '>', '<', 'OP']
	//syms = ['SELECT', 'WHERE', 'AND', 'COL', 'TABLE', 'CAPTION', 'PAGE', 'SECTION', 'OP', 'COND', 'QUESTION', 'AGG', 'AGGOPS', 'CONDOPS']
	
	public static final String[] AGGS = new String[] {"empty", "MAX", "MIN", "COUNT", "SUM", "AVG"}; //only empty for now.
	
	
	public static final String[] ops = {"=", ">", "<"};
	
	public static final String constant = "const";
}
