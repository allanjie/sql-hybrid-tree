package org.statnlp.example.sqlht.wikisql;

import java.util.ArrayList;
import java.util.Arrays;

import org.statnlp.commons.types.Instance;
import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseNetwork;
import org.statnlp.example.base.BaseNetwork.NetworkBuilder;
import org.statnlp.example.sqlht.HybridGrammar;
import org.statnlp.example.sqlht.HybridPattern;
import org.statnlp.example.sqlht.SHTConfig.unit_type;
import org.statnlp.example.sqlht.SQLDataManager;
import org.statnlp.example.sqlht.SQLUnit;
import org.statnlp.example.sqlht.SQLValUnit;
import org.statnlp.example.sqlht.SemanticForest;
import org.statnlp.example.sqlht.SemanticForestNode;
import org.statnlp.hypergraph.LocalNetworkParam;
import org.statnlp.hypergraph.Network;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkException;
import org.statnlp.hypergraph.NetworkIDMapper;




public class WikiNetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = 4835182393678428500L;

	private HybridGrammar _g;
	private SQLDataManager _dm;
	
	private boolean DEBUG = false;

	
	static {
		NetworkIDMapper.setCapacity(new int[]{50, 300, 15, 35000, 10});
	}
	
	public WikiNetworkCompiler(HybridGrammar g, SQLDataManager dm){
		this._g = g;
		this._dm = dm;
//		Collections.sort(this.list, Comparator.comparing(SQLUnit::getType));
	}
	
	private long toNodeRoot (int sentLen) {
		return toNode(0, sentLen - 1, WikiConfig.FOREST_MAX_DEPTH,  this._dm.getAllUnits().size(), 0);
	}
	
	
	private long toNode(int bIndex, int eIndex, SemanticForestNode node, HybridPattern p) {
		//endIndex, span len, 
		return this.toNode(bIndex, eIndex, node.getHIndex(), node.getWIndex(), p.getId());
	}
	
	private long toNode(int bIndex, int eIndex, int hIndex, int unitId, int pId) {
		return NetworkIDMapper.toHybridNodeID(new int[] {
				//the direction becomes the mid index for the full span
				eIndex, eIndex - bIndex, hIndex, unitId, pId
		});
	}
	
	/**
	 * For debug purpose
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unused")
	private String node2Str(long node) {
		StringBuilder sb = new StringBuilder();
		int[] arr = NetworkIDMapper.toHybridNodeArray(node);
		if (arr[3] == this._dm.getAllUnits().size()) return "ROOT";
		sb.append("[");
		sb.append((arr[0] - arr[1]) + "," + (arr[0]) + 
				" unit:" + this._dm.getAllUnits().get(arr[3]) + " " + 
				this._g.getPatternById(arr[4]));
		return sb.toString();
	}
	
	@Override
	public BaseNetwork compileLabeled(int networkId, Instance ins, LocalNetworkParam param) {
		WikiInstance inst = (WikiInstance)ins;
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder();
		Sentence sent = inst.getInput();
		SemanticForest tree = inst.getOutput();
//		System.out.println(sent);
		for(int eIndex=0; eIndex < sent.length(); eIndex++){
			
			for(int L = 0; L<=eIndex; L++){
				int bIndex = eIndex - L;
				//[bIndex, eIndex)
				for (SemanticForestNode forestNode :  tree.getAllNodes()) {
					if (forestNode.isRoot()) continue;
					SQLUnit unit = forestNode.getUnit();
					if(eIndex == bIndex){
//						System.out.println(sent.get(bIndex).getForm() + " " + unit.toString());
						long node = this.toNode(bIndex, eIndex, forestNode, this._g.getw());
						builder.addNode(node);
					}
					
					
					if (forestNode.getChildren().length > 0) {
						SemanticForestNode[] childTreeNodes0 = forestNode.getChildren()[0];
						long node_X = this.toNode(bIndex, eIndex, forestNode, this._g.getX());
						for (SemanticForestNode childNode : childTreeNodes0) {
							boolean added = false;
							long node_child = this.toNode(bIndex, eIndex, childNode, this._g.getRootPatternByArity(1));
//							if (!this._g.getRootPatternByArity(childUnit.arity()).isA() && !this._g.getRootPatternByArity(childUnit.arity()).isB()) {
//								throw new RuntimeException("the following must be a or b");
//							}
//							if (bIndex == eIndex && bIndex == 9 && childUnit.getName().equals("const")) {
//								System.out.println("adding equal sign");
//								System.out.println(node2Str(node_child));
//							}
							if (forestNode.getUnit().getType() == unit_type.aggregation && childNode.getUnit().getType() == unit_type.aggregation) {
								throw new RuntimeException("aggreation continue?");
							}
							if(builder.contains(node_child)){
								if(!added){
									builder.addNode(node_X);
									added = true;
								}
								builder.addEdge(node_X, new long[]{node_child});
							}
						}
					}
					
					
					
					for(HybridPattern lhs : this.getValidHybridPatterns(unit)){
						
						long node = this.toNode(bIndex, eIndex, forestNode, lhs);
						boolean added = false;
						
						ArrayList<HybridPattern[]> RHS = this._g.getRHS(unit.arity(), lhs);
						//no edges to add for this pattern.
						if(lhs.isw()){
							continue;
						}
						if(lhs.isX()){
							continue;
						}
						for(HybridPattern[] rhs : RHS){
							if(rhs.length == 1){
								long node_c1 = this.toNode(bIndex, eIndex, forestNode, rhs[0]);
								
								if (unit.getType() == unit_type.cond_val) {
									if (lhs.isB()) {
										
										if(forestNode.getChildren().length == 1 && !(rhs[0].getForm().equals("WX") ||  rhs[0].getForm().equals("XW")) ) {
											continue;
										}
										if (forestNode.getChildren().length == 0  && ! rhs[0].isW()  ) {
											continue;
										}
									}
								} else {
									if (lhs.isB() && rhs[0].isW()) {
										if (unit.getType() == unit_type.column && forestNode.getChildren().length == 0 ) {
										} else 
											continue;
									}
								}
								
								
								if (lhs.isB() && rhs[0].isW() && forestNode.getChildren().length == 0) {
									if (unit.getType() != unit_type.cond_val && unit.getType() != unit_type.column) continue;
									if (unit.getType() == unit_type.cond_val) {
										StringBuilder seq = new StringBuilder();
										for (int i = bIndex; i <= eIndex; i++) {
											seq.append( i== bIndex ? sent.get(i).getForm() : " " + sent.get(i).getForm());
										}
										SQLValUnit valUnit = (SQLValUnit)unit;
										if (!valUnit.val.equals(seq.toString())) continue;
									}
								}
//								if (lhs.isB( && (rhs[0].getForm().equals("WXW") || rhs[0].isX()) && unit.getType() == unit_type.cond_val) continue;
								
								
								if(builder.contains(node_c1)){
//									if (unit.getType() == unit_type.cond_val && lhs.isB()) {
//										System.out.println(unit.toString() + " " + lhs + " " + Arrays.toString(rhs) + " " + bIndex + " " + eIndex);
//									}
									if(!added){
//										if (bIndex == 2 && eIndex == 8) {
//											System.out.println("yes right 1 " + unit.toString() + " " + lhs + " " + Arrays.toString(rhs));
//										}
										builder.addNode(node);
										added = true;
									}
									builder.addEdge(node, new long[]{node_c1});
								}
							} else if(rhs.length == 2){
								
								for(int cIndex=bIndex; cIndex<eIndex; cIndex++){
									long node_c1 = this.toNode(bIndex, cIndex, forestNode, rhs[0]);
									long node_c2 = this.toNode(cIndex+1, eIndex, forestNode, rhs[1]);
									if(builder.contains(node_c1) && builder.contains(node_c2)){
										
										if (unit_type.cond_val == unit.getType() && forestNode.getChildren().length == 1 && (lhs.getForm().equals("WX") || lhs.getForm().equals("XW")) ) {
											if (  (rhs[0].isW() && rhs[1].isX())   ||  (rhs[0].isX() && rhs[1].isW())  ) {} else continue;
										}
										
										
										
										if (   (rhs[0].isW() && rhs[1].isX())   ||  (rhs[0].isX() && rhs[1].isW()) ) {
											if (!   (unit_type.cond_val == unit.getType() && forestNode.getChildren().length == 1)  )
												continue;
											
//											System.out.println("curennt " + bIndex + " " + cIndex + " " + eIndex);
											int start = rhs[0].isW() ? bIndex : cIndex + 1;
											int end = rhs[0].isW() ? cIndex : eIndex;
											StringBuilder seq = new StringBuilder();
											for (int i = start; i <= end; i++) {
												seq.append( i== start ? sent.get(i).getForm() : " " + sent.get(i).getForm());
											}
											SQLValUnit valUnit = (SQLValUnit)unit;
											if (!valUnit.val.equals(seq.toString())) continue;
											
//											System.out.println(valUnit.val);
//											System.out.println("yes: " + bIndex + " " + cIndex + " " + eIndex + " pattern: " + Arrays.toString(rhs) + " " + valUnit.val);
										}
										
//										if (! (bIndex == cIndex || cIndex+1 == eIndex)) throw new RuntimeException("not 1 word split");
											
										if(!added){
											builder.addNode(node);
											added = true;
										}
										builder.addEdge(node, new long[]{node_c1, node_c2});
									}
								}
							} else {
								throw new RuntimeException("# rhs="+Arrays.toString(rhs));
							}
						}
					}
				}
			}
		}
		
		long root = this.toNodeRoot(sent.length());
		builder.addNode(root);
		
		
		SemanticForestNode[][] children_of_root = tree.getRoot().getChildren();
		
		if(children_of_root.length!=1)
			throw new RuntimeException("The root should have arity 1...");
		
		SemanticForestNode[] child_of_root = children_of_root[0];
		for(int k = 0; k<child_of_root.length; k++){
			long preroot = this.toNode(0, sent.length() - 1, child_of_root[k], this._g.getRootPatternByArity(child_of_root[k].arity()));
			try {
				builder.addEdge(root, new long[] {preroot});
			} catch(NetworkException e) {
				System.err.println("node not found for sent: " + sent.toString());
				System.err.println(tree.toString());
				e.printStackTrace();
			}
			
		}
		
		
		BaseNetwork network = builder.build(networkId, inst, param, this);
//		System.out.println("#nodes (labeled network): " + network.getAllNodes().length);
		if (DEBUG) {
			BaseNetwork unlabeled = this.compileUnlabeled(networkId, inst, param);
			if (!unlabeled.contains(network)) {
				throw new RuntimeException("not contained at: " + inst.getInstanceId() + " " + inst.getInput().toString());
			}
		}
		
		return network;
	}

	@Override
	public BaseNetwork compileUnlabeled(int networkId, Instance ins, LocalNetworkParam param) {
		
		WikiInstance inst = (WikiInstance)ins;
		NetworkBuilder<BaseNetwork> builder = NetworkBuilder.builder();
		Sentence sent = inst.getInput();
		SemanticForest tree = inst.getForest();
//		System.out.println(sent);
		for(int eIndex=0; eIndex < sent.length(); eIndex++){
			
			for(int L = 0; L<=eIndex; L++){
				int bIndex = eIndex - L;
				//[bIndex, eIndex)
				for (SemanticForestNode forestNode :  tree.getAllNodes()) {
					if (forestNode.isRoot()) continue;
					SQLUnit unit = forestNode.getUnit();
					if(eIndex == bIndex){
//						System.out.println(sent.get(bIndex).getForm() + " " + unit.toString());
						long node = this.toNode(bIndex, eIndex, forestNode, this._g.getw());
						builder.addNode(node);
					}
					
					
					if (forestNode.getChildren().length > 0) {
						SemanticForestNode[] childTreeNodes0 = forestNode.getChildren()[0];
						long node_X = this.toNode(bIndex, eIndex, forestNode, this._g.getX());
						for (SemanticForestNode childNode : childTreeNodes0) {
							boolean added = false;
							long node_child = this.toNode(bIndex, eIndex, childNode, this._g.getRootPatternByArity(1));
//							if (!this._g.getRootPatternByArity(childUnit.arity()).isA() && !this._g.getRootPatternByArity(childUnit.arity()).isB()) {
//								throw new RuntimeException("the following must be a or b");
//							}
//							if (bIndex == eIndex && bIndex == 9 && childUnit.getName().equals("const")) {
//								System.out.println("adding equal sign");
//								System.out.println(node2Str(node_child));
//							}
							if(builder.contains(node_child)){
								if(!added){
									builder.addNode(node_X);
									added = true;
								}
								builder.addEdge(node_X, new long[]{node_child});
							}
						}
					}
					
					
					
					for(HybridPattern lhs : this.getValidHybridPatterns(unit)){
						
						long node = this.toNode(bIndex, eIndex, forestNode, lhs);
						boolean added = false;
						
						ArrayList<HybridPattern[]> RHS = this._g.getRHS(unit.arity(), lhs);
						//no edges to add for this pattern.
						if(lhs.isw()){
							continue;
						}
						if(lhs.isX()){
							continue;
						}
						for(HybridPattern[] rhs : RHS){
							if(rhs.length == 1){
								long node_c1 = this.toNode(bIndex, eIndex, forestNode, rhs[0]);
								
								if (unit.getType() == unit_type.cond_val) {
									if (lhs.isB()) {
										
										if(forestNode.getChildren().length == 1 && !(rhs[0].getForm().equals("WX") ||  rhs[0].getForm().equals("XW")) ) {
											continue;
										}
										if (forestNode.getChildren().length == 0  && ! rhs[0].isW()  ) {
											continue;
										}
									}
								} else {
									if (lhs.isB() && rhs[0].isW()) {
										if (unit.getType() == unit_type.column && forestNode.getChildren().length == 0 ) {
											
										} else 
											continue;
									}
								}
								
								
								if (lhs.isB() && rhs[0].isW() && forestNode.getChildren().length == 0) {
									if (unit.getType() != unit_type.cond_val && unit.getType() != unit_type.column) continue;
								}
								
								
								if(builder.contains(node_c1)){
									
									if(!added){
										builder.addNode(node);
										added = true;
									}
									builder.addEdge(node, new long[]{node_c1});
								}
							} else if(rhs.length == 2){
								
								for(int cIndex=bIndex; cIndex<eIndex; cIndex++){
									long node_c1 = this.toNode(bIndex, cIndex, forestNode, rhs[0]);
									long node_c2 = this.toNode(cIndex+1, eIndex, forestNode, rhs[1]);
									if(builder.contains(node_c1) && builder.contains(node_c2)){
										if (unit_type.cond_val == unit.getType() && forestNode.getChildren().length == 1 && (lhs.getForm().equals("WX") || lhs.getForm().equals("XW")) ) {
											if  (  ! ( (rhs[0].isW() && rhs[1].isX())   ||  (rhs[0].isX() && rhs[1].isW()) ) )
												continue;
										}
										
										if (   (rhs[0].isW() && rhs[1].isX())   ||  (rhs[0].isX() && rhs[1].isW()) ) {
											if (!   (unit_type.cond_val == unit.getType() && forestNode.getChildren().length == 1)  )
												continue;
										}
										if(!added){
											builder.addNode(node);
											added = true;
										}
										builder.addEdge(node, new long[]{node_c1, node_c2});
									}
								}
							} else {
								throw new RuntimeException("# rhs="+Arrays.toString(rhs));
							}
						}
					}
				}
			}
		}
		
		long root = this.toNodeRoot(sent.length());
		builder.addNode(root);
		
		SemanticForestNode[][] children_of_root = tree.getRoot().getChildren();
		
		if(children_of_root.length!=1)
			throw new RuntimeException("The root should have arity 1...");
		
		SemanticForestNode[] child_of_root = children_of_root[0];
		for(int k = 0; k<child_of_root.length; k++){
			long preroot = this.toNode(0, sent.length() - 1, child_of_root[k], this._g.getRootPatternByArity(child_of_root[k].arity()));
			if (builder.contains(preroot))
				builder.addEdge(root, new long[] {preroot});
		}
		
		
		BaseNetwork network = builder.build(networkId, inst, param, this);
		return network;
	}
	
	private HybridPattern[] getValidHybridPatterns(SQLUnit unit){
		HybridPattern[] ps = this._g.getPatternsByArity(unit.arity());
		return ps;
	}

	@Override
	public Instance decompile(Network network) {
		BaseNetwork stNetwork = (BaseNetwork)network;
		WikiInstance inst = (WikiInstance)stNetwork.getInstance();
		System.err.println(stNetwork.getMax() + " id: " + inst.getInstanceId() + " " + inst.getInput().toString());
		//if the value is -inf, it means there is no prediction.
		if(stNetwork.getMax()==Double.NEGATIVE_INFINITY){
			return inst;
		}
		SemanticForest prediction = this.toTree(stNetwork); 
		inst.setPrediction(prediction);
		return inst;
	}
	
	private SemanticForest toTree(BaseNetwork network){
		SemanticForestNode root = SemanticForestNode.createRootNode(WikiConfig.FOREST_MAX_DEPTH);
		this.toTree_helper(network, network.countNodes()-1, root);
		return new SemanticForest(root);
	}
	
	//eIndex, eIndex - bIndex, direction, hIndex, wIndex, pId, nodeType.ordinal(), k
	private void toTree_helper(BaseNetwork network, int node_k, SemanticForestNode currNode){
		int[] ids_node = network.getNodeArray(node_k);
		int[] children_k = network.getMaxPath(node_k);
		double score = network.getMax(node_k);
		
		if(currNode.getScore()==Double.NEGATIVE_INFINITY){
			currNode.setScore(score);
			currNode.setInfo("info:"+Arrays.toString(ids_node));
		}
		
		WikiInstance inst = (WikiInstance)(network.getInstance());
		Sentence sent = inst.getInput();
		
		
		
		for (int child_k : children_k) {
			int[] ids_child = network.getNodeArray(child_k);
			if (node_k == network.countNodes() - 1) {
				SQLUnit unit = this._dm.getAllUnits().get(ids_child[3]);
				SemanticForestNode childNode = new SemanticForestNode(unit, ids_child[2], 1);
//				System.err.println("1:"+currNode.getUnit()+"\t"+currNode.getUnit().arity());
				currNode.setChildren(0, new SemanticForestNode[]{childNode});
				
				this.toTree_helper(network, child_k, childNode);
				
			} else if (this._g.getX().getId() == ids_node[4]) { //means the child is B or A.
				SQLUnit unit = this._dm.getAllUnits().get(ids_child[3]);
				unit_type type = unit.getType();
//				System.out.println("decompile x: " + unit.toString() + " " + type);
				SemanticForestNode childNode = null;
				if (type == unit_type.cond_val ) {
//						System.out.println("decompiling");
					int end = ids_child[0];
					int begin = ids_child[0] - ids_child[1];
					StringBuilder seq = new StringBuilder();
					for (int i = begin; i <= end; i++) {
						seq.append( i== begin ? sent.get(i).getForm() : " " + sent.get(i).getForm());
					}
					unit = new SQLValUnit(unit, seq.toString());
				}
				childNode = new SemanticForestNode(unit, ids_child[2], 1);
				currNode.setChildren(0, new SemanticForestNode[]{childNode});
				this.toTree_helper(network, child_k, childNode);
				
			} else if (this._g.getPatternById(ids_node[4]).isB()) {
				if (this._g.getPatternById(ids_child[4]).isW()) {
					currNode.clearChildren();
				}
				this.toTree_helper(network, child_k, currNode);
			} else {
				
				if (this._g.getPatternById(ids_node[4]).getForm().equals("XW") || this._g.getPatternById(ids_node[4]).getForm().equals("WX")) {
					
					if (this._g.getPatternById(ids_child[4]).isW()) {
						SQLUnit unit = this._dm.getAllUnits().get(ids_child[3]);
						
						unit_type type = unit.getType();
						if (type == unit_type.cond_val ) {
							int end = ids_child[0];
							int begin = ids_child[0] - ids_child[1];
							StringBuilder seq = new StringBuilder();
							for (int i = begin; i <= end; i++) {
								seq.append( i== begin ? sent.get(i).getForm() : " " + sent.get(i).getForm());
							}
							unit = new SQLValUnit(unit, seq.toString());
							currNode.setUnit(unit);
						} 
					} 
					this.toTree_helper(network, child_k, currNode);
				} else {
					this.toTree_helper(network, child_k, currNode);
				}
				
			}
		}
	}
	
}
