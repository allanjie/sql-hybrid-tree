/** Statistical Natural Language Processing System
    Copyright (C) 2014-2015  Lu, Wei
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.statnlp.example.sqlht.wikisql;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.statnlp.commons.io.RAWF;
import org.statnlp.commons.ml.opt.GradientDescentOptimizer.BestParamCriteria;
import org.statnlp.commons.ml.opt.OptimizerFactory;
import org.statnlp.commons.types.Instance;
import org.statnlp.example.sqlht.DepMLP;
import org.statnlp.example.sqlht.GeoLSTM;
import org.statnlp.example.sqlht.HybridGrammar;
import org.statnlp.example.sqlht.HybridGrammarReader;
import org.statnlp.example.sqlht.SQLDataManager;
import org.statnlp.example.sqlht.SQLValUnit;
import org.statnlp.example.sqlht.SemMetric;
import org.statnlp.example.sqlht.SemanticForest;
import org.statnlp.example.sqlht.emb.WordEmbedding;
import org.statnlp.hypergraph.DiscriminativeNetworkModel;
import org.statnlp.hypergraph.GlobalNetworkParam;
import org.statnlp.hypergraph.NetworkCompiler;
import org.statnlp.hypergraph.NetworkConfig;
import org.statnlp.hypergraph.NetworkConfig.StoppingCriteria;
import org.statnlp.hypergraph.NetworkModel;
import org.statnlp.hypergraph.decoding.Metric;
import org.statnlp.hypergraph.neural.GlobalNeuralNetworkParam;
import org.statnlp.hypergraph.neural.NeuralNetworkCore;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class WikiMain {
	
	public static int numThreads = 8;
	public static String language = "en";
	public static int numIteration = 40000;
	public static double l2 = 0.01;
	public static int trainNum = 100;
	public static int devNum = 100;
	public static int testNum = 100;
	public static boolean saveModel = false;
	public static boolean readModel = false;
	public static String modelFolder = "models/";
	public static StoppingCriteria criteria = StoppingCriteria.SMALL_RELATIVE_CHANGE;
	public static NeuralType nn = NeuralType.none;
	public static OptimizerFactory optimizer = OptimizerFactory.getLBFGSFactory();
	public static int gpuId = -1;
	public static String embedding = "polyglot";
	public static double dropout = 0.0;
	public static int embeddingSize = 64;
	public static int hiddenSize = 0;
	public static boolean evalDev = false;
	public static int evalK = 11;
	public static String type = "none";
	public static boolean fixEmbedding = false; //by default do not fix the embedding.
	public static boolean useEmbeddingFeature = false;
	public static WordEmbedding emb = null;
	public static int epochLim = 1;
	public static boolean saveGMWeightOnly = false; //if saving the graphical model weights only
	public static boolean fixPretrainedWeight = true;
	public static String optimStr = "lbfgs"; 
	public static boolean doTest = true;
	public static boolean saveEpochModel = false;
	public static boolean loadEpochModel = false;
	public static int loadEpochModelNum = -1;
	
	public static boolean useBigram = true;
	
	public static final boolean DEBUG = false; 
	
	public static enum NeuralType {
		mlp, //comes with different type
		lstm,
		none
	}
	
	public static void main(String args[]) throws IOException, InterruptedException, ClassNotFoundException{
		
		System.err.println(WikiMain.class.getCanonicalName());
		processArgs(args);
		
		String lang = language;
		String train_file = "data/annotated/train.jsonl";
//		String dev_file = "data/clinic/dev.tokenized.json";
		String test_file = "data/annotated/test.jsonl";
		String g_filename = "sqlhybridgrammar.txt";
		
		if (DEBUG) {
			train_file = "data/annotated/train.jsonl";
			test_file = "data/annotated/dev.jsonl";
			l2 = 0.01;
			numThreads = 8;
			trainNum = -1;
			testNum = trainNum;
			useBigram = false;
//			test_file = train_file;
		}
		
		NetworkConfig.NUM_THREADS = numThreads;
		NetworkConfig.AVOID_DUPLICATE_FEATURES = true;
		NetworkConfig.L2_REGULARIZATION_CONSTANT = l2;
		
		NetworkConfig.STOPPING_CRITERIA = criteria;
		NetworkConfig.FEATURE_TOUCH_TEST = NetworkConfig.USE_NEURAL_FEATURES ? true : false;
		
		int numIterations = numIteration;
		String modelFile = "sht_l2_"+l2;
		String nnModelFile = "shtnn_"+language+"_"+ nn  + "_"+ embedding + "_" + type + "_hidd_" + hiddenSize + "_"+optimStr + "_batch" + NetworkConfig.BATCH_SIZE+"_fe" + 
				fixEmbedding+ ".m";
		NetworkModel model = null;
		List<WikiInstance> insts_test = null;
		List<WikiInstance> insts_dev = null;
		
		SQLDataManager dm = new SQLDataManager();
		WikiReader reader = new WikiReader(dm);
		if (!readModel) {
			
			
			List<WikiInstance> insts_train = reader.read(train_file, dm,  true, trainNum);
			
			insts_test = reader.read(test_file, dm,  false, testNum);
			insts_dev = evalDev ? reader.read(test_file, dm,  false, devNum) :
				null;
			WikiInstance[] dev_instances = null;
			
			for (WikiInstance inst : insts_train) {
				SemanticForest forest = reader.toForest(dm, reader.getSpecificUnits(inst.getColumns(), dm));
				inst.setForest(forest);
			}
			for (WikiInstance inst : insts_test) {
				SemanticForest forest = reader.toForest(dm, reader.getSpecificUnits(inst.getColumns(), dm));
				inst.setForest(forest);
			}
			if (insts_dev != null) {
				for (WikiInstance inst : insts_dev) {
					SemanticForest forest = reader.toForest(dm, reader.getSpecificUnits(inst.getColumns(), dm));
					inst.setForest(forest);
				}
			}
			
			if (evalDev) {
				dev_instances = new WikiInstance[insts_dev.size()];
				for(int k = 0; k<dev_instances.length; k++){
					dev_instances[k] = insts_dev.get(k);
					dev_instances[k].setUnlabeled();
				}
			}
			dm.printStat();
			int size = insts_train.size();
			
			WikiInstance train_instances[] = new WikiInstance[size];
			for(int k = 0; k<insts_train.size(); k++){
				train_instances[k] = insts_train.get(k);
				train_instances[k].setInstanceId(k);
				train_instances[k].setLabeled();
			}
			
			
			System.err.println("Read.."+train_instances.length+" instances.");
			
			HybridGrammar g = HybridGrammarReader.read(g_filename);
			
			
			List<NeuralNetworkCore> nets = new ArrayList<NeuralNetworkCore>();
			if(NetworkConfig.USE_NEURAL_FEATURES){
				if (nn == NeuralType.mlp) {
					//actually inside is bilinear;
					nets.add(new DepMLP("MultiLayerPerceptron", dm.getAllUnits().size(), hiddenSize, gpuId,
							embedding, fixEmbedding, dropout, lang, type, 1)
							.setModelFile(modelFolder + nnModelFile));
				} else if (nn == NeuralType.lstm) {
					nets.add(new GeoLSTM("SimpleBiLSTM", dm.getAllUnits().size(), hiddenSize, gpuId,
							embedding, fixEmbedding, lang, type)
							.setModelFile(modelFolder + nnModelFile));
				} else {
					throw new RuntimeException("unknown nn type: " + nn);
				}
				
			} 
			GlobalNetworkParam gnp = new GlobalNetworkParam(optimizer, new GlobalNeuralNetworkParam(nets));
			
			WikiFeatureManager fm = new WikiFeatureManager(gnp, g, dm, false);
			
			NetworkCompiler compiler = new WikiNetworkCompiler(g, dm); 
			
			model = DiscriminativeNetworkModel.create(fm, compiler);
			if (evalDev) {
				Function<Instance[], Metric> evalFunc = new Function<Instance[], Metric>(){
					@Override
					public Metric apply(Instance[] t) {
						try {
							return evaluate(t);
						} catch (IOException e) {
							e.printStackTrace();
						}
						return null;
					}
				};
				model.train(train_instances, numIterations, dev_instances, evalFunc, evalK);
			} else {
				model.train(train_instances, numIterations);
//				gnp.getStringIndex().buildReverseIndex();
//				StringIndex strIdx = gnp.getStringIndex();
//				strIdx.buildReverseIndex();
//				gnp.setStoreFeatureReps();
//				for (int i = 0; i < gnp.size(); i++) {
//					int[] fs = gnp.getFeatureRep(i);
//					String type = strIdx.get(fs[0]);
//					String output = strIdx.get(fs[1]);
//					String input = strIdx.get(fs[2]);
//					System.out.println("["+type+", " + output +", " + input + "]" );
//				}
			}
		} else {
			System.out.println("[Info] Reading the model..");
			String file = modelFolder + modelFile;
			if (loadEpochModel) file += loadEpochModelNum;
			ObjectInputStream ois = RAWF.objectReader(file);
			model = (NetworkModel)ois.readObject();
			ois.close();
			WikiFeatureManager sfm = (WikiFeatureManager)model.getFeatureManager();
			insts_test = reader.read(test_file, sfm._dm,  false, testNum);
		}
		
		if (saveModel) {
			//save both stuff
			System.out.println("[Info] Saving the model...");
			ObjectOutputStream oos =  RAWF.objectWriter(modelFolder + modelFile);
			if (NetworkConfig.USE_NEURAL_FEATURES)
				model.getFeatureManager().getParam_G().getNNParamG().getAllNets().get(0).setModelFile(modelFolder + nnModelFile);
			oos.writeObject(model);
			oos.close();
			if (saveGMWeightOnly) {
				oos =  RAWF.objectWriter(modelFolder + modelFile + ".weight");
				oos.writeObject(model.getFeatureManager().getParam_G().getWeights());
				oos.close();
			}
		}
		
		
		if (doTest) {
			SQLValUnit.EVAL = true;
			WikiInstance test_instances[];
			Instance[] output_instances_unlabeled;
			
			test_instances = new WikiInstance[insts_test.size()];
			for(int k = 0; k<test_instances.length; k++){
				test_instances[k] = insts_test.get(k);
				test_instances[k].setUnlabeled();
			}
			output_instances_unlabeled = model.test(test_instances);
			
			evaluate(output_instances_unlabeled);
		}
		
	}
	
	public static SemMetric evaluate(Instance[] output_instances_unlabeled) throws IOException {
		double total = output_instances_unlabeled.length;
		double corr = 0;
		
		
		
		for(int k = 0; k<output_instances_unlabeled.length; k++){
			WikiInstance output_inst_U = (WikiInstance)output_instances_unlabeled[k];
			boolean r = output_inst_U.getOutput().equals(output_inst_U.getPrediction());
			System.out.println(output_inst_U.getInstanceId()+":\t"+r);
			System.out.println("  " + output_inst_U.getInput().toString());
			SemanticForest output = output_inst_U.getOutput();
			System.out.println("  gold: " + output);
			SemanticForest prediction = output_inst_U.getPrediction();
			System.out.println("  prediction: " + prediction);
			if(r){
				corr++;
			}
		}
		
		System.out.println("text accuracy="+corr/total+"="+corr+"/"+total);
		return new SemMetric(corr/total * 100);
	}
	
	private static void processArgs(String[] args) throws IOException, ClassNotFoundException {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("")
				.defaultHelp(true).description("Dependency-based Hybrid Tree for Semantic Parsing");
		
		/****Training configuration***/
		parser.addArgument("-t", "--thread").type(Integer.class).setDefault(numThreads).help("number of threads");
		parser.addArgument("--l2reg", "-l2").type(Double.class).setDefault(l2).help("L2 Regularization");
		parser.addArgument("--iteration", "-iter").type(Integer.class).setDefault(numIteration).help("The number of iteration.");
		parser.addArgument("--saveModel", "-sm").type(Boolean.class).setDefault(saveModel).help("saving the model");
		parser.addArgument("--readModel", "-rm").type(Boolean.class).setDefault(readModel).help("reading the model");
		
		/********Data configuration**********/
		parser.addArgument("--trainNum","-trainN").type(Integer.class).setDefault(trainNum).help("number of training");
		parser.addArgument("--devNum","-devN").type(Integer.class).setDefault(devNum).help("number of development");
		parser.addArgument("--testNum","-testN").type(Integer.class).setDefault(testNum).help("number of test data");
		
		
		parser.addArgument("--stopCriteria", "-sc").type(StoppingCriteria.class).setDefault(criteria).help("stopping criteria");
		parser.addArgument("--neural", "-nn").type(NeuralType.class).setDefault(nn).help("neural network type");
		parser.addArgument("--batch", "-b").type(Integer.class).setDefault(NetworkConfig.BATCH_SIZE).help("batch size");
		parser.addArgument("--useBatch", "-ub").type(Boolean.class).setDefault(NetworkConfig.USE_BATCH_TRAINING).help("use batch training");
		parser.addArgument("-optim", "--optimizer").type(String.class).choices("lbfgs", "sgdclip", "adam").setDefault("lbfgs").help("optimizer");
		parser.addArgument("-gi", "--gpuid").type(Integer.class).setDefault(gpuId).help("gpuid");
		parser.addArgument("-emb", "--embedding").type(String.class).choices("glove", "polyglot", "random", "fasttext").setDefault(embedding).help("embedding to use");
		parser.addArgument("-do", "--dropout").type(Double.class).setDefault(dropout).help("dropout rate for the lstm");
		parser.addArgument("-es", "--embeddingSize").type(Integer.class).setDefault(embeddingSize).help("embedding size");
		parser.addArgument("-hs", "--hiddenSize").type(Integer.class).setDefault(hiddenSize).help("hidden size");
		parser.addArgument("-ed", "--evalDev").type(Boolean.class).setDefault(evalDev).help("evaluate on dev set");
		parser.addArgument("-ef", "--evalFreq").type(Integer.class).setDefault(evalK).help("evaluation frequency");
		parser.addArgument("--type").type(String.class).setDefault(type).help("the type for MLP, can be mlp or bilinear or bilinear-mlp");
		parser.addArgument("-fe", "--fixEmbedding").type(Boolean.class).setDefault(fixEmbedding).help("if fix the embedding");
		parser.addArgument("-uef", "--useEmbFeats").type(Boolean.class).setDefault(useEmbeddingFeature).help("use the embedding value as features");
		parser.addArgument("-el", "--epochLim").type(Integer.class).setDefault(epochLim).help("evaluate the dev set after a number of epochs");
		parser.addArgument("-sgmw", "--saveGMWeight").type(Boolean.class).setDefault(saveGMWeightOnly).help("save graphical model weights only");
		parser.addArgument("-fpw", "--fixPretrainedWeight").type(Boolean.class).setDefault(fixPretrainedWeight).help("fix the pretrained feature weight");
		parser.addArgument("-dt", "--dotest").type(Boolean.class).setDefault(doTest).help("test the data");
		parser.addArgument("-regn", "--regneural").type(Boolean.class).setDefault(NetworkConfig.REGULARIZE_NEURAL_FEATURES).help("regularizing the neural features");
		parser.addArgument("-ubg", "--useBigram").type(Boolean.class).setDefault(useBigram).help("add bigram  features");
		parser.addArgument("-sem", "--saveEpochModel").type(Boolean.class).setDefault(saveEpochModel).help("save the models in epoch");
		parser.addArgument("-lem", "--loadEpochModel").type(Boolean.class).setDefault(loadEpochModel).help("load the epcoch model");
		parser.addArgument("-lemn", "--loadEpochModelNum").type(Integer.class).setDefault(loadEpochModelNum).help("load the epoch model num");
		
		parser.addArgument("-lpw", "--loadPretrainWeights").type(Boolean.class).setDefault(NetworkConfig.LOAD_MODEL_WEIGHTS).help("load pretrained model weights");
		Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        numThreads = ns.getInt("thread");
        l2 = ns.getDouble("l2reg");
        numIteration = ns.getInt("iteration");
        language = ns.getString("language");
        
        trainNum = ns.getInt("trainNum");
        devNum = ns.getInt("devNum");
        testNum = ns.getInt("testNum");
        
        saveModel = ns.getBoolean("saveModel");
        readModel = ns.getBoolean("readModel");
        criteria = (StoppingCriteria)ns.get("stopCriteria");
        nn = (NeuralType)ns.get("neural");
        if (nn != NeuralType.none) {
        	NetworkConfig.USE_NEURAL_FEATURES = true;
        	NetworkConfig.REGULARIZE_NEURAL_FEATURES = ns.getBoolean("regneural");
        	NetworkConfig.IS_INDEXED_NEURAL_FEATURES = false;
        }
        NetworkConfig.USE_BATCH_TRAINING = ns.getBoolean("useBatch");
        if (NetworkConfig.USE_BATCH_TRAINING) {
        	NetworkConfig.RANDOM_BATCH = true;
        	NetworkConfig.BATCH_SIZE = ns.getInt("batch");
        }
        gpuId = ns.getInt("gpuid");
        embedding = ns.getString("embedding");
        dropout = ns.getDouble("dropout");
        embeddingSize = ns.getInt("embeddingSize");
        hiddenSize = ns.getInt("hiddenSize");
        evalDev = ns.getBoolean("evalDev");
        evalK = ns.getInt("evalFreq");
        String optim = ns.getString("optimizer");
        switch (optim) {
        	case "lbfgs": optimizer = OptimizerFactory.getLBFGSFactory(); optimStr = "lbfgs"; break;
        	case "sgdclip": optimizer = evalDev ? OptimizerFactory.getGradientDescentFactoryUsingGradientClipping(BestParamCriteria.BEST_ON_DEV, 0.05, 5): 
        				OptimizerFactory.getGradientDescentFactoryUsingGradientClipping(BestParamCriteria.LAST_UPDATE, 0.05, 5);
        				optimStr = "sgdclip";
        							break;
        	case "adam" : optimizer = evalDev ? OptimizerFactory.getGradientDescentFactoryUsingAdaM(BestParamCriteria.BEST_ON_DEV) :
        		OptimizerFactory.getGradientDescentFactoryUsingAdaM(BestParamCriteria.LAST_UPDATE) ; 
        		optimStr = "adam";
        		break;
        	default: optimizer = OptimizerFactory.getLBFGSFactory(); break;
        }
        type = ns.getString("type");
        fixEmbedding = ns.getBoolean("fixEmbedding");
        useEmbeddingFeature = ns.getBoolean("useEmbFeats");
        epochLim = ns.getInt("epochLim");
        saveGMWeightOnly = ns.getBoolean("saveGMWeight");
        doTest = ns.getBoolean("dotest");
        useBigram = ns.getBoolean("useBigram");
        saveEpochModel = ns.getBoolean("saveEpochModel");
        loadEpochModel = ns.getBoolean("loadEpochModel");
        loadEpochModelNum = ns.getInt("loadEpochModelNum");
        NetworkConfig.LOAD_MODEL_WEIGHTS = ns.getBoolean("loadPretrainWeights");
        for (String key : ns.getAttrs().keySet()) {
        	System.err.println(key + ": " + ns.get(key));
        }
        if (saveModel && readModel) {
        	throw new RuntimeException("cannot save model and read model at the same time.");
        }
	}
}