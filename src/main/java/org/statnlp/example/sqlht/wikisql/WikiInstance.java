/** Statistical Natural Language Processing System
    Copyright (C) 2014-2015  Lu, Wei

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.statnlp.example.sqlht.wikisql;

import org.statnlp.commons.types.Sentence;
import org.statnlp.example.base.BaseInstance;
import org.statnlp.example.sqlht.SQLUnit;
import org.statnlp.example.sqlht.SemanticForest;


/**
 * @author wei_lu
 *
 */
public class WikiInstance extends BaseInstance<WikiInstance, Sentence, SemanticForest>{
	
	private static final long serialVersionUID = -8190693110092491424L;
	
	
	
	private SQLUnit[] alignments;
	
	private String[] columns;
	
	private SemanticForest forest;
	
	public WikiInstance(int instanceId, double weight) {
		super(instanceId, weight);
	}
	
	public WikiInstance(int instanceId, double weight, Sentence input, SemanticForest output, String[] columns, SemanticForest forest) {
		super(instanceId, weight);
		this.input = input;
		this.output = output;
		this.columns = columns;
		this.forest = forest;
	}
	
	@Override
	public int size() {
		return this.input.length();
	}
	
	public SemanticForest getForest() {
		return this.forest;
	}
	
	public void setForest(SemanticForest forest) {
		 this.forest = forest;
	}
	
	public String[] getColumns() {
		return this.columns;
	}
	
	public WikiInstance duplicate() {
		WikiInstance result = new WikiInstance(this._instanceId, this._weight, this.input, this.output, this.columns, this.forest);
		return result;
	}
	
	public void setAlignments(SQLUnit[] alignments) {
		this.alignments = alignments;
	}
	
	public SQLUnit[] getAlignments() {
		return this.alignments;
	}
}
