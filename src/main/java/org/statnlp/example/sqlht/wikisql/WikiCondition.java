package org.statnlp.example.sqlht.wikisql;

public class WikiCondition{

	
	
	public String operation;
	
	public String column;
	
	public String val;
	
	int idx;
	
	public WikiCondition(String column, String operation, String val) {
		this.operation = operation;
		this.column = column;
		this.val = val;
	}
	
	public void setIdx(int idx) {
		 this.idx = idx;
	}

	@Override
	public String toString() {
		return "Condition [column=" +column + ", operation=" + operation + ", val=" + val
				+ "]";
	}
	
	public int getIdx() {
		return this.idx;
	}
}
