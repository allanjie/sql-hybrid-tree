package org.statnlp.example.sqlht.wikisql;

import cern.colt.Arrays;

public class WikiSQL {

	
	public String tabId;
	public String column;
	public String aggregation;
	public WikiCondition[] conditions;
	
	public WikiSQL(String aggregation, String table, String column, WikiCondition[] conds) {
		this.aggregation = aggregation;
		this.tabId = table;
		this.column = column;
		this.conditions = conds;
	}

	@Override
	public String toString() {
		return "SQL [table=" + tabId + ", column=" + column + ", aggregation=" + aggregation + ", condition="
				+ Arrays.toString(conditions) + "]";
	}
	
	public int size() {
		return 2 + conditions.length * 3;
	}
	
}
