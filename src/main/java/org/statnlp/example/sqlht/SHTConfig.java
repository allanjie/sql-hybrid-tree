package org.statnlp.example.sqlht;

public class SHTConfig {

	public static final String SEP = " ### ";
	public static final String dummyUnit = "dummy";
	
	public static int _SEMANTIC_FOREST_MAX_DEPTH = 13; //the max depth of the forest when creating the semantic forest.
	
	public static int _SEMANTIC_COL_MAX_DEPTH = 3;
	public static int _SEMANTIC_COND_MAX_DEPTH = 9;
	
	public static final String[] AGGS = new String[] {"empty", "COUNT", "MAX", "MIN", "AVG"}; //only empty for now.
	
	public static final String[] tables = {"DEMOGRAPHIC", "DIAGNOSES", "PROCEDURES", "PRESCRIPTIONS", "LAB"};
	
	
	public static final String[][] cols = {
			{"SUBJECT_ID","HADM_ID","NAME","MARITAL_STATUS","AGE","DOB","GENDER","LANGUAGE","RELIGION","ADMISSION_TYPE",
				"DAYS_STAY","INSURANCE","ETHNICITY","EXPIRE_FLAG","ADMISSION_LOCATION","DISCHARGE_LOCATION","DIAGNOSIS","DOD","DOB_YEAR","DOD_YEAR","ADMITTIME","DISCHTIME","ADMITYEAR"},
			{"SUBJECT_ID","HADM_ID","ICD9_CODE","SHORT_TITLE","LONG_TITLE"},
			{"SUBJECT_ID","HADM_ID","ICD9_CODE","SHORT_TITLE","LONG_TITLE"},
			{"SUBJECT_ID", "HADM_ID", "ICUSTAY_ID", "DRUG_TYPE", "DRUG","FORMULARY_DRUG_CD","ROUTE","DRUG_DOSE"},
			{"SUBJECT_ID", "HADM_ID", "ITEMID", "CHARTTIME", "FLAG", "VALUE_UNIT", "LABEL","FLUID", "CATEGORY"}
	};
	
	public static final String[] ops = {"=", ">", "<", ">=", "<="};
	
	public static enum unit_type{
		cond_val,
		cond_op,
		cond_col,
		column,
		aggregation,
		root_type;
		
	};
	
	public static final String con = "const";
}
