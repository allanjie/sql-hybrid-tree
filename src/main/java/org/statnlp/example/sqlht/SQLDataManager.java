/** Statistical Natural Language Processing System
    Copyright (C) 2014-2015  Lu, Wei

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.statnlp.example.sqlht;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.statnlp.example.sqlht.SHTConfig.unit_type;



/**
 * @author wei_lu
 *
 */
public class SQLDataManager implements Serializable{
	
	private static final long serialVersionUID = 7131629077691962523L;
	
	private HashMap<SQLUnit, SQLUnit> _unitMap;
	private ArrayList<SQLUnit> _unitList;
	
	private ArrayList<SQLUnit> _rootUnits;
	
	
	private Map<unit_type, List<SQLUnit>> type2Units;
	
	private boolean _semanticUnitsFixed = false;
	
	private HashMap<SQLUnit, ArrayList<SQLUnit>> _validUnitPair_0;
	
	public SQLDataManager(){
		this._unitMap = new HashMap<SQLUnit, SQLUnit>();
		this._unitList = new ArrayList<SQLUnit>();
		
		this._rootUnits = new ArrayList<SQLUnit>();
		
		this.type2Units = new HashMap<>();
		
		this._validUnitPair_0 = new HashMap<>();
		
	}
	
	public void addType2Unit(unit_type type, SQLUnit unit) {
		if (this.type2Units.containsKey(type)) {
			if (!this.type2Units.get(type).contains(unit)) {
				this.type2Units.get(type).add(unit);
			}
		} else {
			List<SQLUnit> list = new ArrayList<>();
			list.add(unit);
			this.type2Units.put(type, list);
		}
	}
	
	public List<SQLUnit> getChildrenUnits(unit_type type) {
		return this.type2Units.get(type);
	}
	
	public void addValidUnitPair(SQLUnit parent, SQLUnit child, int index){
		HashMap<SQLUnit, ArrayList<SQLUnit>> map = this._validUnitPair_0;
		if(!map.containsKey(parent))
			map.put(parent, new ArrayList<SQLUnit>());
		
		ArrayList<SQLUnit> children = map.get(parent);
		if(!children.contains(child)){
			children.add(child);
		}
	}
	
	public boolean isValidUnitPair(SQLUnit parent, SQLUnit child){
		HashMap<SQLUnit, ArrayList<SQLUnit>> map = this._validUnitPair_0;
		if(!map.containsKey(parent))
			return false;
		
		
		ArrayList<SQLUnit> children = map.get(parent);
		return children.contains(child);
	}
	
	public void recordRootUnit(SQLUnit unit){
		int index = this._rootUnits.indexOf(unit);
		if(index<0){
			this._rootUnits.add(unit);
		}
	}
	
	public ArrayList<SQLUnit> getRootUnits(){
		return this._rootUnits;
	}
	
	public ArrayList<SQLUnit> getAllUnits(){
		return this._unitList;
	}
	
	public SQLUnit toSemanticUnit(String name, unit_type type){
		SQLUnit unit = new SQLUnit(name, type);
		if(this._unitMap.containsKey(unit))
			return this._unitMap.get(unit);
		if(this._semanticUnitsFixed){
			return unit;
		}
		unit.setId(this._unitList.size());
		this._unitList.add(unit);
		this._unitMap.put(unit, unit);
		return unit;
	}
	
	public void fixSemanticUnits(){
		this._semanticUnitsFixed = true;
	}
	
	public void printStat() {
		System.out.println("num units: " + this._unitList.size());
		System.out.println("unit map");
		System.out.println(this._unitMap);
		System.out.println("unit list");
		System.out.println(this._unitList);
		System.out.println("root units");
		System.out.println(this._rootUnits.toString());
	}
}
