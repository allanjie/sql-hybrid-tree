#!/bin/bash


### DATA CONFIG
trainNum=-1
devNum=-1
testNum=-1

### TRAINING CONFIG
l2=0.01
mainClass=org.statnlp.example.sqlht.SQLHybridTreeMain
jarFile=sql-ht-1.0.jar
saveModel=true
readModel=false
thread=40
iters=5000
bigram=true
multiTable=true
mwe=med
evalDev=false
evalFreq=50

##LOGGING
logFile=sqlht_${trainNum}_${l2}_sm_${saveModel}_rm_${readModel}_nofix_bigram_${bigram}_multiTable_${multiTable}_mwe_${mwe}_new_dev.log

java -cp ${jarFile} ${mainClass} --trainNum ${trainNum} \
    --devNum ${devNum} \
    --testNum ${testNum} \
    -l2 ${l2} -mwe ${mwe} \
    -sm ${saveModel} \
    -rm ${readModel} --evalDev ${evalDev} --evalFreq ${evalFreq} \
    -t ${thread} -ubg ${bigram} \
    -iter ${iters} --multiTable ${multiTable} > logs/${logFile} 2>&1 

