#!/bin/bash


### DATA CONFIG
trainNum=1000
devNum=-1
testNum=-1

### TRAINING CONFIG
l2=0.01
mainClass=org.statnlp.example.sqlht.wikisql.WikiMain
jarFile=sql-ht-1.0.jar
saveModel=true
readModel=false
thread=40
iters=5000
bigram=false

##LOGGING
logFile=wikisql_${trainNum}_${l2}_sm_${saveModel}_rm_${readModel}_nofix_bigram_${bigram}.log

java -cp ${jarFile} ${mainClass} --trainNum ${trainNum} \
    --devNum ${devNum} \
    --testNum ${testNum} \
    -l2 ${l2} \
    -sm ${saveModel} \
    -rm ${readModel} \
    -t ${thread} -ubg ${bigram} \
    -iter ${iters}  > logs/${logFile} 2>&1 

